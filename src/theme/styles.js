import { StyleSheet } from 'react-native';
import * as colors from './colors';

// eslint-disable-next-line import/prefer-default-export
export const CardStyle = StyleSheet.create({
    container: {
        paddingVertical: 15,
    },
    row: {
        flexDirection: 'row-reverse',
        // padding: 10,
        padding: 5,
    },
});


const FloatingButtonSize = 50;
const leftPadding = 20;
const bottomPadding = leftPadding * 1.5;
export const FloatingButtonStyle = StyleSheet.create({
    container: {
        position: 'absolute',
        left: leftPadding,
        bottom: bottomPadding,
        width: FloatingButtonSize,
        height: FloatingButtonSize,
        borderRadius: FloatingButtonSize / 2,
        backgroundColor: colors.Secondary,
        elevation: 4,
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 0,
        }
    }
});

export const shadowStyles = {
    small: {
        elevation: 2,
        shadowOpacity: 0.1,
    },
    medium: {
        elevation: 4,
        shadowOpacity: 0.3,
    },
    center: {
        shadowOffset: {
            width: 0,
            height: 0,
        },
    },
    down: {
        shadowOffset: {
            width: 0,
            height: 5,
        }
    }
};
