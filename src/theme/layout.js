export const smallMargin = 5;
export const largeMargin = 15;
export const mediumMargin = 10;
export const center = {
  justifyContent: "center",
  alignItems: "center"
};
