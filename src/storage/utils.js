import AsyncStorage from "@react-native-community/async-storage";

export const getBoolean = async (key: String) => {
  const res = await AsyncStorage.getItem(key);
  if (!res) {
    return false;
  }
  return JSON.parse(res);
};

export const setObject = (key: String) => async (value: Boolean) => {
  AsyncStorage.setItem(key, JSON.stringify(value));
};
