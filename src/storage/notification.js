import AsyncStorage from "@react-native-community/async-storage";
import { setObject, getBoolean } from "./utils";

const DONT_ASK_AGEINT = "NOTIFICATION_DONT_ASK_AGAIN";
const NOTICATION_SET = "NOTIFICATION_SET";

export const isDontAskAgain = () => AsyncStorage.getItem(DONT_ASK_AGEINT);
export const setDontAskAgain = (value: Boolean) =>
  AsyncStorage.setItem(DONT_ASK_AGEINT, value);

export const isNotificationSet = () => getBoolean(NOTICATION_SET);
export const setNotificationSet = setObject(NOTICATION_SET);
