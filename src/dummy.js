export type RequestItemType = {
    _id: String,
    user: {
        image: String,
        name: String,
    },
    lat: Number,
    long: Number,
    time: Date,
    game: String,
    console: String,
    duration: Number,
    // bids: {
    //     n: Number,
    //     average: Number,
    // }
    price: Number,
}


// eslint-disable-next-line import/prefer-default-export
export const feeds: [RequestItemType] = [
    {
        user: {
            image: 'https://pooyaharatian.me/index_files/img_avatar.JPG',
            name: 'پویا هراتیان نژادی'
        },
        lat: 35.7301,
        long: 51.3723,
        time: new Date(2019, 5, 12),
        game: 'GTA V',
        console: 'PS4',
        duration: 21,
        // bids: {
        //     n: 3,
        //     average: 25000,
        // },
        price: 25000,
        _id: 'sfea',
    },
    {
        user: {
            // image: 'https://pooyaharatian.me/index_files/img_avatar.JPG',
            name: 'سروش یوسفیان'
        },
        lat: 35.7201,
        long: 51.5723,
        time: new Date(2019, 5, 2),
        game: 'Red Dead Redemption Red Dead ',
        console: 'XBOX',
        duration: 60,
        // bids: {
        //     n: 3,
        //     average: 25000,
        // },
        price: 54000,
        _id: 'sfwea',
    }
];
