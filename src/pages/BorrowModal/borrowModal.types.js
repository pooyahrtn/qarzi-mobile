import { NavigationScreenProp } from "react-navigation";

export type BorrowModalItem = {
  game: String,
  console: String,
  duration: String,
  price: String,
  name: String,
  needIdCard: Boolean
};
export type BorrowModalParams = {
  item: BorrowModalItem
};

export type Props = {
  navigation: NavigationScreenProp<{}, BorrowModalParams>
};

export type BorrowModalViewProps = {
  item: BorrowModalItem,
  duration: Number,
  changeDuration: (newValue: Number) => void,
  navigation: NavigationScreenProp,
  loadingNewPost: Boolean,
  onSubmitPressed: () => void,
  submitted: Boolean,
  durationError: Boolean
};
