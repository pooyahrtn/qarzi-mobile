import React from "react";
import {
  StyleSheet,
  View,
  Keyboard,
  TouchableWithoutFeedback
} from "react-native";
import { Text, Devider, KeyboardAvoiding, TextInput } from "../../components";
import SubmitModal from "../../components/MSubmitModal";
import { BorrowModalViewProps } from "./borrowModal.types";
import { layout as gLayout, colors } from "../../theme";
import { toPrice, parseNumber, toPersian } from "../../utils/text";
import locales from "../../locales";

function borrowText(game, gameConsole, price, name) {
  return `آیا تمایل به اجاره ی بازی
${game}
برای کنسول ${gameConsole}
به ازای روزی ${price} تومان
از ${name || ""} دارید؟`;
}

function getName(first_name, last_name) {
  if (first_name && last_name) {
    return `${first_name} ${last_name}`;
  }
  if (first_name) {
    return first_name;
  }
  if (last_name) {
    return last_name;
  }
  return "وی";
}

export default function(props: BorrowModalViewProps) {
  const {
    navigation,
    submitted,
    onSubmitPressed,
    duration,
    changeDuration,
    loadingNewPost,
    durationError,
    item: {
      user: { first_name, last_name },
      price,
      price_per_day,
      game,
      console: gameConsole,
      needIdCard
    }
  } = props;
  const name = getName(first_name, last_name);
  const needIdCardText = `${name} درخواست ارائه ی کارت ملی برای اجاره دادن کرده.`;
  const priceText =
    duration.length > 0
      ? `به مبلغ ${toPersian(
          toPrice(parseNumber(duration) * price_per_day)
        )} تومان`
      : "";

  return (
    <KeyboardAvoiding behavior="position">
      <SubmitModal
        navigation={navigation}
        title="درخواست اجاره"
        submitted={submitted}
        onSubmit={onSubmitPressed}
        loadingPost={loadingNewPost}
      >
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View>
            <Text style={styles.mainText} transformNumbers>
              {borrowText(game, gameConsole, price, name)}
            </Text>
            <Devider style={{ marginVertical: 10 }} />
            {needIdCard && (
              <Text style={styles.needIdCardText}>{needIdCardText}</Text>
            )}
            <Text style={styles.mainText}>برای چند روز میخواهید؟</Text>
            {durationError && (
              <Text style={styles.durationError}>{locales.durationNeeded}</Text>
            )}
            <View style={styles.daysInputContainer}>
              <TextInput
                containerStyle={styles.daysTextInput}
                label="مدت"
                prefix={`روز ${priceText}`}
                keyboardType="numeric"
                value={duration}
                onChangeText={changeDuration}
                error={durationError ? " " : ""}
              />
            </View>

            <Text style={styles.warningText}>
              اپلیکیشن جارنت هیچگونه مسئولیتی در قبال معامله ی شما ندارد.
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </SubmitModal>
    </KeyboardAvoiding>
  );
}

const marginHorizontal = gLayout.mediumMargin;
const marginVertical = gLayout.largeMargin;
const styles = StyleSheet.create({
  mainText: {
    marginHorizontal: gLayout.largeMargin,
    lineHeight: 35
  },
  needIdCardContainer: {
    flexDirection: "row-reverse",
    marginHorizontal,
    marginVertical,
    alignItems: "center"
  },
  needIdCardText: {
    marginHorizontal,
    color: colors.Secondary
  },
  warningText: {
    color: "gray",
    margin: marginVertical,
    fontSize: 14
  },
  daysInputContainer: {
    flexDirection: "row-reverse",
    paddingHorizontal: gLayout.largeMargin,
    minHeight: 40,
    alignItems: "center"
  },
  daysTextInput: {
    width: "100%"
  },
  durationError: {
    margin: marginHorizontal,
    color: "red"
  },
  priceText: {
    color: colors.Primary,
    marginHorizontal
  }
});
