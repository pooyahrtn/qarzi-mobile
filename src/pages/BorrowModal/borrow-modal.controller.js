// @flow
import React, { useState, useEffect } from "react";
import BorrowModalView from "./borrowModal.screen";
import { OutcomeSuggestsContainer } from "../../containers/Suggests/suggests.container";
import wrapContainer from "../../containers/wrapContainers";
import { Props } from "./borrowModal.types";
import { showSuccessSnack } from "../../router/navigate";
import { toEnglish } from "../../utils/text";

function BorrowModalController(props: Props) {
  const {
    navigation,
    navigation: {
      state: {
        params: { item }
      }
    },
    container: {
      state: { loadingNewPost },
      isItemRequested,
      addNewBorrowSuggest
    }
  } = props;
  const [itemRequested, setItemRequested] = useState(false);
  const [duration, setDuration] = useState("");
  const [durationError, setDurationError] = useState(false);

  useEffect(() => {
    isItemRequested(item.id).then(isRequested => setItemRequested(isRequested));
  });

  const sendRequest = async () => {
    if (duration.length === 0) {
      setDurationError(true);
      return;
    }
    const res = await addNewBorrowSuggest({
      feed_id: item.id,
      duration: toEnglish(duration)
    });
    if (res) {
      showSuccessSnack(navigation, () => navigation.goBack());
    }
  };

  return (
    <BorrowModalView
      item={item}
      navigation={navigation}
      duration={duration}
      changeDuration={setDuration}
      loadingNewPost={loadingNewPost}
      onSubmitPressed={sendRequest}
      submitted={itemRequested}
      durationError={durationError}
    />
  );
}

export default wrapContainer(
  BorrowModalController,
  {},
  {
    name: "container",
    container: OutcomeSuggestsContainer
  }
);
