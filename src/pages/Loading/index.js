import wrapContainers from '../../containers/wrapContainers';
import AppContainer from '../../containers/App/app.container';
import LoadingPage from './loading.page';

export default wrapContainers(LoadingPage, {}, {
    name: 'appContainer',
    container: AppContainer,
});
