import React, { useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import AppContainer from '../../containers/App/app.container';
import {
  navigateToApp,
  navigateToWelcome,
  navigateToRequestNotification,
  navigateToRequestLocation
} from '../../router/navigate';
import * as NotificationStore from '../../storage/notification';

type Props = {
  appContainer: AppContainer
};

export default (props: Props) => {
  const {
    navigation,
    appContainer: { loadSavedData }
  } = props;
  useEffect(() => {
    (async () => {
      await loadSavedData();
      const {
        appContainer: {
          state: { login, dontAskLocation, allowedLocation }
        }
      } = props;
      if (login === 'just-in') {
        navigateToWelcome(navigation);
        return;
      }
      const isNotificationSet = await NotificationStore.isNotificationSet();
      const isNotificationDontAskAgaing = await NotificationStore.isDontAskAgain();
      if (!isNotificationSet && !isNotificationDontAskAgaing) {
        navigateToRequestNotification(navigation);
        return;
      }
      if (!dontAskLocation && !allowedLocation) {
        navigateToRequestLocation(navigation);
        return;
      }
      if (login === 'signed-in') {
        navigateToApp(navigation);
        return;
      }
      navigateToWelcome(navigation);
    })();
  }, []);

  return <View style={styles.screen} />;
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: 'white'
  }
});
