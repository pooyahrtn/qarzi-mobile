import React from "react";
import { View, TouchableOpacity } from "react-native";
import { Avatar, Icon, Text, Devider } from "../../components";
import styles from "./profile.styles";
import getThumbnail from "../../utils/getThumbnailLocation";

type HeaderProfileProps = {
  firstName: String,
  lastName: String,
  image: String,
  openSettingScreen: () => void
};

export default function HeaderProfile(props: HeaderProfileProps) {
  const { firstName, lastName, image, openSettingScreen } = props;
  return (
    <View>
      <View style={styles.profileContainer}>
        <Avatar
          containerStyle={styles.avatar}
          image={image}
          thumbnailUri={getThumbnail(image)}
        />
        <View style={styles.profileNameContainer}>
          <Text style={styles.nameText}>
            {`${firstName || ""} ${lastName || ""}`}
          </Text>
          <TouchableOpacity
            style={styles.settingsButton}
            onPress={openSettingScreen}
          >
            <Icon name="settings" iconType="feathers" size={15} />
            <Text style={styles.settingsButtonText}>تنظیمات</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Devider style={{ margin: 10 }} />
    </View>
  );
}
