import React from 'react';
import { FlatList, View } from 'react-native';
import { Page, Text, Icon } from '../../components';
import HeaderProfile from './HeaderProfile';
import RequestItem from '../../components/RequestItem';
import ProfileContainer from '../../containers/Profile/profile.container';
import MyPostsContainer from '../../containers/MyPosts/my-posts.container';
import { navigateToSettings, openFeedOptionModals } from '../../router/navigate';
import { transformMyPost } from '../../serializers/request.serializer';
import styles from './profile.styles';
import { colors } from '../../theme';

type Props = {
  profileContainer: ProfileContainer,
  myPostsContainer: MyPostsContainer
};
export default class ProfileScreen extends React.Component<Props> {
  componentDidMount() {
    const {
      profileContainer: { loadProfile },
      myPostsContainer: { initPage }
    } = this.props;
    loadProfile();
    initPage();
  }

  render() {
    const {
      profileContainer: {
        state: {
          profile: { first_name, last_name, image }
        }
      },
      myPostsContainer: {
        state: { data, refreshing },
        loadMore,
        refreshFeeds
      },
      navigation
    } = this.props;

    return (
      <Page
        header={{
          title: 'پروفایل من'
        }}
      >
        <FlatList
          ListHeaderComponent={
            <HeaderProfile
              firstName={first_name}
              lastName={last_name}
              image={image}
              openSettingScreen={() => navigateToSettings(navigation)}
            />
          }
          data={data}
          refreshing={refreshing}
          // contentContainerStyle={{ flex: 1 }}
          onRefresh={refreshFeeds}
          ListEmptyComponent={<EmptyList loading={refreshing} />}
          showsVerticalScrollIndicator={false}
          onEndReached={loadMore}
          renderItem={({ item }) => (
            <RequestItem
              {...transformMyPost(item)}
              verbose
              onOptionPressed={() => openFeedOptionModals(navigation, item.id, { isMine: true })}
            />
          )}
          keyExtractor={item => item.id}
        />
      </Page>
    );
  }
}

function EmptyList(props: { loading: boolean }) {
  const { loading } = props;
  return (
    <View style={styles.emptyContainer}>
      {!loading && (
        <>
          <Icon name="feather" iconType="feathers" size={70} color={colors.Secondary} />
          <Text style={styles.emptyText}>پست های شما، اینجا خواهند آمد</Text>
        </>
      )}
    </View>
  );
}
