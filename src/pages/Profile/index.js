import wrapContainer from "../../containers/wrapContainers";
import ProfileContainer from "../../containers/Profile/profile.container";
import MyPostsConainer from "../../containers/MyPosts/my-posts.container";
import ProfileScreen from "./profile.screen";

export default wrapContainer(
  ProfileScreen,
  {},
  { name: "profileContainer", container: ProfileContainer },
  { name: "myPostsContainer", container: MyPostsConainer }
);
