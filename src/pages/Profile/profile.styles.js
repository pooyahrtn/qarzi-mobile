import { StyleSheet } from 'react-native';
import { styles as gStyles, layout as gLayout } from '../../theme';

const AVARAT_SIZE = 100;

export default StyleSheet.create({
  screen: {
    flex: 1,
    zIndex: 100
  },
  profileContainer: {
    flexDirection: 'row-reverse',
    paddingVertical: gLayout.largeMargin,
    paddingHorizontal: gLayout.mediumMargin,
    margin: gLayout.mediumMargin,
    backgroundColor: 'white',
    borderRadius: 10,
    ...gStyles.shadowStyles.small,
    ...gStyles.shadowStyles.center
  },
  avatar: {
    width: AVARAT_SIZE,
    height: AVARAT_SIZE,
    borderRadius: AVARAT_SIZE / 2
  },
  profileNameContainer: {
    flex: 1,
    justifyContent: 'space-between',
    marginEnd: gLayout.mediumMargin
  },
  settingsButton: {
    borderWidth: 0.5,
    borderColor: 'rgb(200,200,200)',
    borderRadius: 5,
    height: 30,
    flexDirection: 'row-reverse',
    ...gLayout.center,
    paddingHorizontal: 10
  },
  settingsButtonText: {
    marginRight: 5
  },
  nameText: {
    textAlign: 'center',
    fontSize: 16
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingVertical: 30
  },
  emptyText: {
    margin: 10
  }
});
