import { StyleSheet } from 'react-native';
import { colors, styles as gStyles, layout as gLayout } from '../../theme';

const AVATAR_SIZE = 140;

export default StyleSheet.create({
  screen: {
    flex: 1
  },
  avatarContainer: {
    padding: gLayout.largeMargin,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE / 2,
    ...gStyles.shadowStyles.center,
    ...gStyles.shadowStyles.small
  },
  avatarChangeText: {
    marginVertical: gLayout.mediumMargin,
    color: 'gray'
  },
  nameInputContainer: {
    margin: gLayout.smallMargin,
    marginHorizontal: gLayout.largeMargin,
    paddingVertical: gLayout.smallMargin
  },
  nameContainer: {
    borderRadius: gLayout.largeMargin,
    margin: gLayout.largeMargin,
    backgroundColor: 'white',
    ...gStyles.shadowStyles.center,
    ...gStyles.shadowStyles.small
  },
  saveButton: {
    backgroundColor: colors.Thrinity,
    borderRadius: 5,
    margin: gLayout.mediumMargin,
    padding: 5,
    width: '25%',
    ...gStyles.shadowStyles.small,
    ...gStyles.shadowStyles.center,
    shadowColor: colors.Thrinity,
    shadowOpacity: 0.3
  },
  saveButtonText: {
    color: 'white'
  },
  inlineRowContainer: {
    borderColor: 'rgb(200,200,200)',
    paddingVertical: gLayout.largeMargin,
    marginHorizontal: gLayout.largeMargin,
    borderBottomWidth: 0.5
  },
  inlineRowTop: {
    borderTopWidth: 0.5,
    marginTop: gLayout.largeMargin
  },
  inlineRowBottom: {
    borderBottomWidth: 0
  },
  exitButtonText: {
    color: colors.Secondary
  },
  toggleRowContainer: {
    flexDirection: 'row-reverse',
    alignItems: 'center'
  },
  inlineTitleText: {
    flex: 1
    // fontWeight: 'bold'
  },
  inlineDescriptionText: {
    color: 'gray',
    fontSize: 13
  },
  supportText: {
    color: colors.Thrinity
  }
});
