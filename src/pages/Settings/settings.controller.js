import React from 'react';
import { Linking } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import SettingScreen from './settings.screen';
import MyProfileContainer from '../../containers/Profile/profile.container';
import AppContainer from '../../containers/App/app.container';
import wrapConainers from '../../containers/wrapContainers';
import { navigateToWelcome } from '../../router/navigate';

type Props = {
  profileContainer: MyProfileContainer,
  appContainer: AppContainer
};
function SettingsScreenController(props: Props) {
  const {
    profileContainer: {
      state: {
        profile: { first_name, last_name, image },
        loadingSaveProfile
      },
      updateProfilePicture,
      updateProfile,
      deleteProfile
    },
    appContainer: {
      logout: setContainerLogout,
      setAllowLocation,
      state: { allowedLocation }
    },
    navigation
  } = props;
  const logout = async () => {
    await setContainerLogout();
    navigateToWelcome(navigation);
  };
  const deleteAccount = async () => {
    const deleteSuccessfully = await deleteProfile();
    if (deleteSuccessfully) {
      await logout();
    }
  };
  const openAndUpdateImage = async () => {
    const imageFile = await openCropPicker();
    const { path, mime } = imageFile;
    await updateProfilePicture(path, mime);
  };
  const saveProfile = async (firstName, lastName) => {
    await updateProfile({ first_name: firstName, last_name: lastName });
    navigation.goBack();
  };
  return (
    <SettingScreen
      firstName={first_name}
      lastName={last_name}
      image={image}
      logout={logout}
      onSupportPress={mailTo}
      openCropPicker={openAndUpdateImage}
      loadingSave={loadingSaveProfile}
      saveProfile={saveProfile}
      allowLocation={allowedLocation}
      setAllowLocation={setAllowLocation}
      deleteAccount={deleteAccount}
    />
  );
}

export default wrapConainers(
  SettingsScreenController,
  {},
  {
    name: 'profileContainer',
    container: MyProfileContainer
  },
  {
    name: 'appContainer',
    container: AppContainer
  }
);

const openCropPicker = () =>
  ImagePicker.openPicker({
    width: 400,
    height: 400,
    cropping: true,
    cropperCircleOverlay: true
  });

function mailTo() {
  Linking.openURL('mailto:contact@jarent.app?subject=پشتیبانی');
}
