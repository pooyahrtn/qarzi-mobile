// @flow
import React, { useState } from 'react';
import { View, TouchableOpacity, ScrollView, SafeAreaView, Switch } from 'react-native';
import R from 'ramda';
import { Header, Avatar, Text, TextInput, LoadingButton } from '../../components';
import styles from './settings.styles';
import DontConfirmModal from './DontConfirmModal';
import locales from '../../locales';

export type Props = {
  image: String,
  firstName: string,
  lastName: string,
  allowLocation: boolean,
  setAllowLocation: (allow: boolean) => void,
  logout: () => void,
  openCropPicker: () => void,
  loadingSave: Boolean,
  saveProfile: (firstName: string, lastName: string) => void,
  deleteAccount: () => void,
  onSupportPress: () => void
};

export default function SettingsScreen(props: Props) {
  const {
    image,
    firstName: savedFirstName,
    lastName: savedLastName,
    logout,
    openCropPicker,
    saveProfile,
    loadingSave,
    allowLocation,
    setAllowLocation,
    deleteAccount,
    onSupportPress
  } = props;
  const [firstName, setFirstName] = useState(savedFirstName);
  const [lastName, setlastName] = useState(savedLastName);
  const [logoutModalVisible, setLogoutModalVisible] = useState(false);
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);
  const openLogoutModal = R.compose(
    setLogoutModalVisible,
    R.T
  );
  const closeLogoutModal = R.compose(
    setLogoutModalVisible,
    R.F
  );

  const openDeleteModal = () => setDeleteModalVisible(true);
  const closeDeleteModal = () => setDeleteModalVisible(false);

  return (
    <View style={styles.screen}>
      <DontConfirmModal
        isVisible={logoutModalVisible}
        message={locales.logoutMessage}
        confirmText={locales.logout}
        closeModal={closeLogoutModal}
        confirmPressed={logout}
      />
      <DontConfirmModal
        isVisible={deleteModalVisible}
        message={locales.deleteAccountMessage}
        confirmText={locales.deleteAccount}
        closeModal={closeDeleteModal}
        confirmPressed={deleteAccount}
      />
      <Header
        title="تنظیمات"
        actionButton={
          <LoadingButton
            style={styles.saveButton}
            loading={loadingSave}
            onPress={() => saveProfile(firstName, lastName)}
          >
            <Text style={styles.saveButtonText}>ذخیره</Text>
          </LoadingButton>
        }
      />
      <ScrollView>
        <SafeAreaView>
          <TouchableOpacity style={styles.avatarContainer} onPress={openCropPicker}>
            <Avatar image={image} containerStyle={styles.avatar} iconSize={40} />
            <Text style={styles.avatarChangeText}>تغییر عکس</Text>
          </TouchableOpacity>
          <View style={styles.nameContainer}>
            <TextInput
              label="نام"
              containerStyle={styles.nameInputContainer}
              value={firstName}
              onChangeText={setFirstName}
            />
            <TextInput
              label="نام خانوادگی"
              value={lastName}
              onChangeText={setlastName}
              containerStyle={styles.nameInputContainer}
            />
          </View>
          <View style={[styles.inlineRowContainer, styles.inlineRowTop, styles.inlineRowBottom]}>
            <View style={styles.toggleRowContainer}>
              <Text style={styles.inlineTitleText}>دسترسی به موقعیت جغرافیایی</Text>
              <Switch value={allowLocation} onValueChange={setAllowLocation} />
            </View>
            <Text style={styles.inlineDescriptionText}>{locales.locationSettingDescriptiom}</Text>
          </View>
          <TouchableOpacity
            style={[styles.inlineRowContainer, styles.inlineRowTop]}
            onPress={onSupportPress}
          >
            <Text style={[styles.exitButtonText, styles.supportText]}>تماس با پشتیبانی</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.inlineRowContainer]} onPress={openLogoutModal}>
            <Text style={styles.exitButtonText}>خروج از حساب کاربری</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.inlineRowContainer, styles.inlineRowBottom]}
            onPress={openDeleteModal}
          >
            <Text style={styles.exitButtonText}>حذف حساب کاربری</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </ScrollView>
    </View>
  );
}
