import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Text, ModalView } from "../../components";
import { layout as glayout, colors } from "../../theme";

export type Props = {
  isVisible: Boolean,
  message: String,
  confirmText: String,
  closeModal: () => void,
  confirmPressed: () => void
};

export default (props: Props) => {
  const { isVisible, message, confirmText, closeModal, confirmPressed } = props;
  return (
    <ModalView
      isVisible={isVisible}
      containerStyle={styles.container}
      onBackdropPress={closeModal}
    >
      <Text>{message}</Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={[styles.button, styles.rejectButton]}
          onPress={closeModal}
        >
          <Text style={[styles.buttonText, styles.rejectButtonText]}>
            انصراف
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.confirmButton]}
          onPress={confirmPressed}
        >
          <Text style={[styles.buttonText, styles.confirmButtonText]}>
            {confirmText}
          </Text>
        </TouchableOpacity>
      </View>
    </ModalView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: glayout.largeMargin
  },
  buttonContainer: {
    marginTop: glayout.largeMargin,
    flexDirection: "row-reverse",
    alignItems: "center",
    justifyContent: "space-between"
  },
  button: {
    borderRadius: 10,
    alignItems: "center",
    padding: glayout.smallMargin
  },
  confirmButton: {
    marginRight: glayout.largeMargin
  },
  confirmButtonText: {
    color: colors.Secondary
  },
  rejectButton: {
    backgroundColor: colors.Thrinity,
    flex: 1
  },
  buttonText: {
    fontSize: 17
  },
  rejectButtonText: {
    color: "white"
  }
});
