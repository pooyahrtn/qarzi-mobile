import React, { useState } from "react";
import wrapContainers from "../../containers/wrapContainers";
import LendContainer from "../../containers/Lend/lend.container";
import BorrowContainer from "../../containers/Borrow/borrow.container";
import AppContainer from "../../containers/App/app.container";
import NewPostScreeen from "./newpost.screen";
import { serializeLend, serializeBorrow } from "./newpost.serializer";

type Props = {
  lendContainer: LendContainer,
  borrowContainer: BorrowContainer,
  appContainer: AppContainer
};

const NewPostController = (props: Props) => {
  const {
    navigation,
    lendContainer: {
      state: { loadingNewPost: lendLoading },
      addNewPost: addNewLend
    },
    borrowContainer: {
      state: { loadingNewPost: borrowLoading },
      addNewPost: addNewBorrow
    },
    appContainer: {
      state: { lat, long }
    }
  } = props;

  const [intention, setIntention] = useState();
  const [gameConsole, setGameConsole] = useState();
  const [price, setPrice] = useState();
  const [duration, setDuration] = useState();
  const [name, setName] = useState();
  const [needIdCard, setneedIdCard] = useState(true);

  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const hideSuccessModal = () => setShowSuccessModal(false);
  const toggleNeedIdCard = () => setneedIdCard(!needIdCard);
  const state = {
    intention,
    gameConsole,
    price,
    duration,
    name,
    needIdCard,
    lat,
    long
  };
  const isFormValid = validateForm(state);
  const onSendPressed = async () => {
    if (!isFormValid) {
      return;
    }
    let res;
    if (intention === "bo") {
      const body = serializeLend(state);
      res = await addNewLend(body);
    } else {
      const body = serializeBorrow(state);
      res = await addNewBorrow(body);
    }
    if (res) {
      setShowSuccessModal(true);
    }
  };

  return (
    <NewPostScreeen
      navigation={navigation}
      intention={intention}
      setIntention={setIntention}
      price={price}
      setPrice={setPrice}
      name={name}
      setName={setName}
      duration={duration}
      setDuration={setDuration}
      gameConsole={gameConsole}
      setGameConsole={setGameConsole}
      needIdCard={needIdCard}
      toggleNeedIdCard={toggleNeedIdCard}
      formIsValid={isFormValid}
      onSendPressed={onSendPressed}
      loadingNewPost={lendLoading || borrowLoading}
      showSuccessModal={showSuccessModal}
      hideSuccessModal={hideSuccessModal}
      onSuccessModalHide={() => navigation.goBack("")}
    />
  );
};

function validateForm(state) {
  const { intention, name, gameConsole, duration, price } = state;
  return (
    intention &&
    name &&
    gameConsole &&
    price &&
    (intention === "le" || (intention === "bo" && duration))
  );
}

export default wrapContainers(
  NewPostController,
  {},
  {
    name: "borrowContainer",
    container: BorrowContainer
  },
  {
    name: "appContainer",
    container: AppContainer
  },
  {
    name: "lendContainer",
    container: LendContainer
  }
);
