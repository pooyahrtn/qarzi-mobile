import { StyleSheet, Platform } from "react-native";
import { colors, layout as gLayouts, styles as gStyles } from "../../theme";

const marginHorizontal = gLayouts.largeMargin;
const marginVertical = 14;
const grayBorderColor = "rgb(200,200,200)";
const darkGray = "rgb(80,80,80)";
const minHeight = 40;

export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "rgb(250,250,250)"
  },
  container: {
    backgroundColor: "white",
    borderRadius: 15,
    borderStyle: "dashed",
    borderColor: "gray",
    paddingVertical: 10,
    borderWidth: 1,
    margin: gLayouts.mediumMargin,
    ...gStyles.shadowStyles.center,
    ...gStyles.shadowStyles.small
  },
  formSectionContainer: {
    margin: gLayouts.largeMargin,
    borderWidth: 0.5,
    borderRadius: 10,
    borderColor: grayBorderColor
  },
  formSectionInnerContainer: {
    flexDirection: "row-reverse",
    alignItems: "center"
  },
  formSectionTitleText: {
    marginHorizontal,
    marginVertical: 7
  },
  formHintText: {
    marginHorizontal,
    marginVertical,
    color: "gray"
  },
  selectIntentionButton: {
    borderWidth: 1,
    flex: 1,
    borderRadius: 6,
    marginHorizontal,
    marginBottom: marginHorizontal,
    marginVertical: marginVertical / 2,
    padding: 8,
    borderColor: grayBorderColor,
    minHeight,
    justifyContent: "center",
    alignItems: "center"
  },
  selectIntentionButtonActive: {
    // backgroundColor: colors.Primary,
    borderColor: colors.Secondary,
    borderWidth: 1.2,
    padding: 7.8
  },
  selectIntentionButtonText: {
    textAlign: "center",
    fontSize: 15,
    color: darkGray
  },
  selectIntentionButtonTextActive: {
    // color: 'white',
    color: colors.Secondary,
    // fontWeight: "bold"
  },
  textInput: {
    // color: ,
    fontSize: 17,
    textTransform: "capitalize"
  },
  textInputContainer: {
    // minHeight,
    marginHorizontal,
    marginVertical: marginVertical / 2
  },
  durationTextInput: {
    // textAlign: "center"
  },
  durationTextContainer: {
    flex: 1
  },
  gameNameTextInput: {
    flex: 1
  },
  priceTextInput: {
    textAlign: "left",
    letterSpacing: 4,
    fontFamily: Platform.OS === "ios" ? "Courier" : "monospace",
    color: "black"
  },
  priceTextInputContainer: {
    flex: 1
  },
  sendButton: {
    margin: gLayouts.mediumMargin,
    justifyContent: "center",
    paddingVertical: gLayouts.mediumMargin,
    backgroundColor: "white",
    borderRadius: 15,
    ...gStyles.shadowStyles.center,
    ...gStyles.shadowStyles.small
  },
  sendText: {
    textAlign: "center",
    // fontWeight: "bold",
    fontSize: 18,
    color: "gray"
  },
  sendTextEnabled: {
    color: "white"
  },
  sendButtonEnabled: {
    backgroundColor: colors.Primary,
    shadowColor: colors.Primary,
    shadowOpacity: 0.5
  },
  needIdCardContainer: {
    flexDirection: "row-reverse",
    marginHorizontal,
    marginVertical,
    alignItems: "center"
  },
  needIdCardText: {
    marginHorizontal
  }
});
