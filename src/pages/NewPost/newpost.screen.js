import React from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  // UIManager,
  // LayoutAnimation,
  // Platform
} from "react-native";
import {
  Header,
  Text,
  TextInput,
  Checkbox,
  PriceTextInput,
  // KeyboardAvoiding,
  LoadingButton
} from "../../components";
import styles from "./newpost.styles";
import { openSelectModal } from "../../router/navigate";
import { colors } from "../../theme";
import SuccessModal from "./SuccessModal";

export type State = {
  intention: "bo" | "le",
  name: String,
  gameConsole: "PS4" | "XBOX" | "PS3",
  duration: String,
  price: String,
  needIdCard: Boolean
};

type Props = State & {
  navigation: any,
  setIntention: (intention: String) => void,
  setGameConsole: (gameConsole: String) => void,
  setPrice: (price: Number) => void,
  setDuration: (duration: Number) => void,
  setName: (name: String) => void,
  toggleNeedIdCard: () => void,
  formIsValid: Boolean,
  loadingNewPost: Boolean,
  onSendPressed: () => void,
  showSuccessModal: Boolean,
  hideSuccessModal: () => void,
  onSuccessModalHide: () => void
};
export default class NewPostScreen extends React.Component<Props> {
  // constructor(props) {
  //   super(props);
  //   // if (Platform.OS === "android") {
  //   //   // eslint-disable-next-line no-unused-expressions
  //   //   UIManager.setLayoutAnimationEnabledExperimental &&
  //   //     UIManager.setLayoutAnimationEnabledExperimental(true);
  //   // }
  // }

  intentionButtonPress = intention => () => {
    const { setIntention } = this.props;
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setIntention(intention);
  };

  selectConsoleButtonPressed = () => {
    const { navigation, setGameConsole } = this.props;
    openSelectModal(navigation, {
      title: "کنسول بازی را انتخاب کنید",
      options: consoleOptions,
      onSelect: key => {
        setGameConsole(key);
      }
    });
  };

  render() {
    const {
      navigation,
      intention,
      gameConsole,
      name,
      setName,
      duration,
      setDuration,
      price,
      setPrice,
      needIdCard,
      toggleNeedIdCard,
      formIsValid,
      loadingNewPost,
      onSendPressed,
      showSuccessModal,
      hideSuccessModal,
      onSuccessModalHide
    } = this.props;
    const needIdCardText = needIdCard ? "دارم" : "ندارم";
    return (
      <View style={styles.screen}>
        <SuccessModal
          isVisible={showSuccessModal}
          onDone={hideSuccessModal}
          onModalHide={onSuccessModalHide}
        />
        <Header
          title="آگهی جدید"
          back={{
            navigation
          }}
        />
        <ScrollView>
          <View style={styles.container}>
            {/* <View> */}
            <FormSection>
              <TextInput
                label="اسم بازی"
                style={[styles.textInput]}
                containerStyle={[
                  styles.textInputContainer,
                  styles.gameNameTextInput
                ]}
                autoFocus
                value={name}
                onChangeText={setName}
              />
            </FormSection>
            <FormSection title="میخوام">
              <IntentionButton
                text="اجاره بدم"
                onPressed={this.intentionButtonPress("le")}
                active={intention === "le"}
              />
              <IntentionButton
                text="اجاره کنم"
                onPressed={this.intentionButtonPress("bo")}
                active={intention === "bo"}
              />
            </FormSection>

            <FormSection title="برای">
              <IntentionButton
                active={gameConsole}
                text={gameConsole || "کنسول بازی را انتخاب کنید"}
                onPressed={this.selectConsoleButtonPressed}
              />
            </FormSection>
            {intention === "bo" && (
              <FormSection>
                <TextInput
                  label="به مدت"
                  containerStyle={[
                    styles.textInputContainer,
                    styles.durationTextContainer
                  ]}
                  style={[styles.textInput, styles.durationTextInput]}
                  keyboardType="numeric"
                  value={duration}
                  prefix="روز"
                  onChangeText={setDuration}
                />
              </FormSection>
            )}

            <FormSection>
              <PriceTextInput
                label="مبلغ"
                style={[styles.textInput, styles.priceTextInput]}
                containerStyle={[
                  styles.textInputContainer,
                  styles.priceTextInputContainer
                ]}
                keyboardType="numeric"
                suffix={intention === "le" ? "تومان در روز" : "تومان"}
                value={price}
                onChangeText={setPrice}
              />
            </FormSection>

            {intention === "le" && (
              <FormSection hint="برای اطمینان بازگشت، میتوانید تقاضای کارت ملی کنید">
                <TouchableOpacity
                  style={styles.needIdCardContainer}
                  onPress={toggleNeedIdCard}
                >
                  <Checkbox checked={needIdCard} activeColor={colors.Primary} />
                  <Text style={styles.needIdCardText}>
                    {`درخواست کارت ملی ${needIdCardText}`}
                  </Text>
                </TouchableOpacity>
              </FormSection>
            )}
            {/* </View> */}
          </View>
          <LoadingButton
            style={[styles.sendButton, formIsValid && styles.sendButtonEnabled]}
            loading={loadingNewPost}
            onPress={onSendPressed}
          >
            <Text
              style={[styles.sendText, formIsValid && styles.sendTextEnabled]}
            >
              ارسال
            </Text>
          </LoadingButton>
        </ScrollView>
      </View>
    );
  }
}
type FormSectionType = {
  title: String,
  hint: String
};
function FormSection(props: FormSectionType) {
  const { title, children, hint } = props;
  return (
    <View style={styles.formSectionContainer}>
      {hint && <Text style={styles.formHintText}>{hint}</Text>}
      {title && <Text style={styles.formSectionTitleText}>{title}</Text>}
      <View style={styles.formSectionInnerContainer}>{children}</View>
    </View>
  );
}

type IntentionButtonType = {
  active: Boolean,
  text: String,
  onPressed: () => void
};

function IntentionButton(props: IntentionButtonType) {
  const { active, text, onPressed } = props;
  return (
    <TouchableOpacity
      onPress={onPressed}
      style={[
        styles.selectIntentionButton,
        active && styles.selectIntentionButtonActive
      ]}
    >
      <Text
        style={[
          styles.selectIntentionButtonText,
          active && styles.selectIntentionButtonTextActive
        ]}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
}

const consoleOptions = [
  {
    key: "PS4",
    value: "PS4"
  },
  {
    key: "XBOX",
    value: "XBOX"
  },
  {
    key: "PS3",
    value: "PS3"
  }
];
