import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import { Text, ModalView } from "../../components";
import { colors, layout as glayouts } from "../../theme";
import locale from "../../locales";

type Props = {
  isVisible: Boolean,
  onDone: () => void,
  onModalHide: () => void
};
export default (props: Props) => {
  const { isVisible, onDone, onModalHide } = props;
  return (
    <ModalView
      isVisible={isVisible}
      onBackdropPress={onDone}
      onModalHide={onModalHide}
    >
      <Text style={styles.messageText}>{locale.successfullySent}</Text>
      <TouchableOpacity onPress={onDone} style={styles.confrimButton}>
        <Text style={styles.okText}>{locale.ok}</Text>
      </TouchableOpacity>
    </ModalView>
  );
};

const styles = StyleSheet.create({
  confrimButton: {
    margin: glayouts.largeMargin,
    borderRadius: glayouts.mediumMargin,
    backgroundColor: colors.Thrinity,
    alignItems: "center",
    padding: glayouts.mediumMargin
  },
  messageText: {
    margin: glayouts.largeMargin
  },
  okText: {
    fontSize: 17,
    color: "white"
  }
});
