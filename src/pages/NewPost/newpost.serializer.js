import { LendType } from '../../containers/Lend/lend.types';
import { BorrowType } from '../../containers/Borrow/borrow.types';
import { State } from './newpost.screen';
import { toEnglish } from '../../utils/text';

function cleanNumberField(value: String) {
  const withoutComma = value ? value.match(/[^,]/g).join('') : '';
  const englished = toEnglish(withoutComma);
  return Number(englished);
}

function mapConsoles(gameConsole: 'PS4' | 'XBOX' | 'PS3') {
  switch (gameConsole) {
    case 'PS3':
      return 'PA';
    case 'PS4':
      return 'PS';
    case 'XBOX':
      return 'XB';
    default:
      return 'PA';
  }
}

function convertLocation(locValue: Number) {
  return locValue.toFixed(4);
}

function serializeCommonField(data: State) {
  const { gameConsole, lat, long, name } = data;
  const locationFields =
    lat && long
      ? {
          lat: convertLocation(lat),
          long: convertLocation(long)
        }
      : {};
  return {
    console: mapConsoles(gameConsole),
    game: name,
    ...locationFields
  };
}

export function serializeLend(data: State): LendType {
  const commonFieldSerialized = serializeCommonField(data);
  const { duration, price } = data;
  return {
    price: cleanNumberField(price),
    duration: toEnglish(duration),
    ...commonFieldSerialized
  };
}

export function serializeBorrow(data: State): BorrowType {
  const commonFieldSerialized = serializeCommonField(data);
  const { price, needIdCard } = data;
  return {
    ...commonFieldSerialized,
    price_per_day: cleanNumberField(price),
    need_id: needIdCard
  };
}
