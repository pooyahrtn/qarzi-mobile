import React from "react";
import { StyleSheet } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import SnackBar from "../components/SnackBar";
import { colors } from "../theme";
import locales from "../locales";

type Props = {
  navigation: NavigationScreenProp<{}, { onEnd: () => void }>
};
export default (props: Props) => {
  const { navigation } = props;
  const onEnd = navigation.getParam("onEnd");
  return (
    <SnackBar
      navigation={navigation}
      message={locales.generalSuccess}
      containerStyle={styles.container}
      onEnd={onEnd}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.Thrinity
  }
});
