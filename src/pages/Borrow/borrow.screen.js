/* eslint-disable no-underscore-dangle */
import React from "react";

import RequestItem from "../../components/RequestItem";

import { trasnformBorrow } from "../../serializers/request.serializer";

import FeedScreen, { Props as FeedProps } from "../Abstract/Feeds/feeds.screen";

type Props = FeedProps & {
  onBorrowButtonPressed: (item: any) => void,
  openFeedOptions: (item: any) => void
};
export default (props: Props) => {
  const {
    lat,
    long,
    myId,
    onBorrowButtonPressed,
    openFeedOptions,
    ...restProps
  } = props;
  return (
    <FeedScreen
      title="اجاره کن"
      renderItem={item => (
        <RequestItem
          data={trasnformBorrow(item, { lat, long }, myId)}
          priceUnit="perday"
          actionButton={{
            title: "میخوام",
            onPress: () => {
              onBorrowButtonPressed(trasnformBorrow(item));
            }
          }}
          onOptionPressed={() => openFeedOptions(item)}
        />
      )}
      {...restProps}
    />
  );
};
