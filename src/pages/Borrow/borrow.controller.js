import React, { useEffect } from 'react';
import LendScreen from './borrow.screen';
import BorrowContainner from '../../containers/Borrow/borrow.container';
import AppContainer from '../../containers/App/app.container';
import wrapContainer from '../../containers/wrapContainers';
import { openAddNewPost, openBorrowModal, openFeedOptionModals } from '../../router/navigate';

type Props = {
  borrowContainner: BorrowContainner,
  appStore: AppContainer
};

function BorrowScreenContainer(props: Props) {
  const {
    appStore: {
      state: { lat, long, id: myId },
      refreshLocation
    },
    borrowContainner: {
      state: { refreshing, data },
      loadMore,
      refreshFeeds,
      initPage
    },
    navigation
  } = props;

  useEffect(() => {
    initPage();
    refreshLocation();
  }, []);
  const onAddButtonPressed = () => {
    openAddNewPost(navigation);
  };

  const onBorrowButtonPressed = item => {
    openBorrowModal(navigation, {
      item
    });
  };

  return (
    <LendScreen
      lat={lat}
      long={long}
      onAddButtonPressed={onAddButtonPressed}
      onBorrowButtonPressed={onBorrowButtonPressed}
      loadMore={loadMore}
      refreshFeeds={refreshFeeds}
      refreshing={refreshing}
      data={data}
      myId={myId}
      openFeedOptions={({ id }) => openFeedOptionModals(navigation, id, { isMine: false })}
    />
  );
}

export default wrapContainer(
  BorrowScreenContainer,
  {},
  {
    name: 'borrowContainner',
    container: BorrowContainner
  },
  {
    name: 'appStore',
    container: AppContainer
  }
);
