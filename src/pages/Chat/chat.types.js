import { OutcomeSuggestsContainer } from "../../containers/Suggests/suggests.container";
import AppContainer from "../../containers/App/app.container";

export type OutcomeControllerProps = {
  appContainer: AppContainer,
  container: OutcomeSuggestsContainer
};
