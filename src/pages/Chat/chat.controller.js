import React, { useEffect } from 'react';
import ChatScreen from './chat.screen';
import {
  OutcomeSuggestsContainer,
  IncomeSuggestsContainer
} from '../../containers/Suggests/suggests.container';
import AppContainer from '../../containers/App/app.container';
import wrapContainer from '../../containers/wrapContainers';
import { OutcomeControllerProps } from './chat.types';
import ChatItem from './ChatItem';

const suggestController = (type: 'in' | 'out') =>
  function OutComeController(props: OutcomeControllerProps) {
    const {
      container: {
        state: { data, refreshing },
        loadMore,
        initPage,
        refreshFeeds
      }
    } = props;
    useEffect(() => {
      initPage();
    }, []);
    return (
      <ChatScreen
        data={data}
        refreshing={refreshing}
        onRefresh={refreshFeeds}
        onEndReached={loadMore}
        renderItem={({ item }) => <ChatItem data={item} noUser={type === 'in'} />}
      />
    );
  };

// function IncomeController(props: OutcomeControllerProps) {
//   const {
//     container: {
//       state: { data, refreshing },
//       loadMore,
//       initPage
//     }
//   } = props;
//   useEffect(() => {
//     initPage();
//   }, []);
//   return (
//     <ChatScreen
//       data={data}
//       refreshing={refreshing}
//       onEndReached={loadMore}
//       renderItem={({ item }) => <OutcomeItem data={item} />}
//     />
//   );
// }

export const InComeSuggests = wrapContainer(
  suggestController('in'),
  {},
  {
    container: IncomeSuggestsContainer,
    name: 'container'
  },
  {
    name: 'appContainer',
    container: AppContainer
  }
);

export const OutComeSuggests = wrapContainer(
  suggestController('out'),
  {},
  {
    container: OutcomeSuggestsContainer,
    name: 'container'
  },
  {
    name: 'appContainer',
    container: AppContainer
  }
);
