import React from 'react';
import { View, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import { Text, Icon, ProgressiveImage } from '../../components';
import { styles as gStyles, layout as gLayouts, colors } from '../../theme';
// import UserRow, { Props as UserProps } from '../../components/UserRow';
import RequestItem from '../../components/RequestItem';
import { transformMyPost } from '../../serializers/request.serializer';
import { toPrice } from '../../utils/text';
import getImageThumbnail from '../../utils/getThumbnailLocation';

export type SuggestProps = {
  type: 'LendSuggest' | 'BorrowSuggest',
  feed: any,
  need_id: Boolean,
  duration: Number,
  from_user: {
    username: String,
    first_name: String,
    last_name: String,
    image: String
  }
};

export type OutcomeRequestProps = {
  data: SuggestProps
};
export type Props = {
  data: SuggestProps,
  noUser: Boolean
};

const intentionText = type => (type === 'BorrowSuggest' ? 'اجاره' : 'اجاره');
const suggestText = type => (type === 'BorrowSuggest' ? 'درخواست' : 'پیشنهاد');
const headerText = type => `${suggestText(type)} ${intentionText(type)}`;
const iconName = type => (type === 'BorrowSuggest' ? 'arrow-down' : 'arrow-up');
const durationText = duration => `به مدت ${duration} روز،`;
const durationPrice = (duration, price_per_day) =>
  `مبلغ ${toPrice(duration * price_per_day)} تومان`;

const formatPhone = (phoneNumber: String) => `0${phoneNumber.slice(4)}`;
function callPhone(phoneNumber) {
  Linking.openURL(`tel:${formatPhone(phoneNumber)}`);
}

export default function ChatItem(props: Props) {
  const {
    data: { feed, type, duration, need_id, from_user },
    noUser
  } = props;
  const { price_per_day } = feed;

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Icon iconType="feathers" color={colors.Secondary} name={iconName(type)} size={17} />
        <Text style={styles.headerText}>{headerText(type)}</Text>
      </View>
      {from_user && <IncomeUser user={from_user} />}
      {/* <Devider style={styles.devider} /> */}
      <RequestItem
        {...transformMyPost(feed)}
        verbose
        noUser={noUser}
        containerStyle={styles.postContainer}
      />
      {/* <Devider style={styles.devider} /> */}
      {duration && (
        <Text transformNumbers style={styles.headerText}>
          {durationText(duration)}
          {durationPrice(duration, price_per_day)}
        </Text>
      )}
      {need_id && <Text style={styles.headerText}>تقاضای کارت ملی دارم</Text>}
      {from_user && from_user.username && (
        <TouchableOpacity
          style={styles.callButton}
          onPress={() => {
            callPhone(from_user.username);
          }}
        >
          <Text style={styles.callButtonText}>تماس</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const userName = (first_name, last_name) => {
  if (first_name && last_name) {
    return `${first_name} ${last_name}`;
  }
  if (first_name) {
    return first_name;
  }
  if (last_name) {
    return last_name;
  }
  return 'شخصی';
};

type IncomeUserProps = {
  user: {
    first_name: String,
    last_name: String,
    image: String
  },
  type: String
};
function IncomeUser(props: IncomeUserProps) {
  const {
    user: { image, first_name, last_name },
    type
  } = props;
  const name = userName(first_name, last_name);
  return (
    <View style={styles.incomeUserContainer}>
      {image && (
        <ProgressiveImage
          source={{ uri: image }}
          style={styles.avatar}
          thumbnailSource={{ uri: getImageThumbnail(image) }}
        />
      )}
      <Text style={styles.messageText}>
        {`${name} برای پست شما ${suggestText(type)} ${intentionText(type)} داده است.`}
      </Text>
    </View>
  );
}

const AVATAR_SIZE = 48;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: gLayouts.mediumMargin,
    marginVertical: gLayouts.mediumMargin,
    borderRadius: gLayouts.mediumMargin,
    backgroundColor: 'white',
    ...gStyles.shadowStyles.center,
    ...gStyles.shadowStyles.small,
    paddingHorizontal: gLayouts.mediumMargin,
    paddingVertical: gLayouts.largeMargin
  },
  headerContainer: {
    marginStart: gLayouts.largeMargin / 2,
    // paddingTop: gLayouts.mediumMargin / 2,
    // paddingBottom: gLayouts.mediumMargin / 2,
    marginBottom: gLayouts.mediumMargin / 2,
    flexDirection: 'row-reverse',
    alignItems: 'center'
  },
  headerText: {
    margin: gLayouts.mediumMargin
  },
  shift: {
    marginRight: 30,
    marginVertical: 15
  },
  devider: {
    height: 0.7,
    backgroundColor: 'rgb(200,200,200)',
    marginVertical: gLayouts.mediumMargin
  },
  postContainer: {
    marginRight: gLayouts.mediumMargin,
    marginBottom: 10,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: 'rgb(200,200,200)',
    borderRadius: 10,
    padding: gLayouts.smallMargin
  },
  incomeUserContainer: {
    flexDirection: 'row-reverse',
    margin: gLayouts.mediumMargin
  },
  avatar: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE / 2
  },
  messageText: {
    marginHorizontal: gLayouts.mediumMargin,
    flex: 1
  },
  callButton: {
    ...gStyles.shadowStyles.center,
    ...gStyles.shadowStyles.small,
    backgroundColor: colors.Thrinity,
    shadowColor: colors.Thrinity,
    borderRadius: gLayouts.mediumMargin,
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 0.5,
    padding: gLayouts.mediumMargin,
    margin: gLayouts.smallMargin
  },
  callButtonText: {
    color: 'white',
    fontSize: 16,
    // fontWeight: 'bold'
  }
});
