import React from 'react';
import { StyleSheet, FlatList, View, FlatListProps } from 'react-native';
import { Text, Icon } from '../../components';
import { colors } from '../../theme';

type Props = {} & FlatListProps;

function EmptyList(props: { loading: boolean }) {
  const { loading } = props;
  return (
    <View style={styles.emptyContainer}>
      {!loading && (
        <>
          <Icon name="message-circle" iconType="feathers" size={70} color={colors.Secondary} />
          <Text style={styles.emptyText}>درخواست ها و اعلانات شما در این قسمت خواهد آمد</Text>
        </>
      )}
    </View>
  );
}

export default (props: Props) => {
  const { refreshing } = props;
  return (
    <FlatList
      contentContainerStyle={{ flexGrow: 1 }}
      ListEmptyComponent={<EmptyList loading={refreshing} />}
      keyExtractor={item => item.id}
      {...props}
    />
  );
};

const styles = StyleSheet.create({
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30
  },
  emptyText: {
    margin: 10
  }
});
