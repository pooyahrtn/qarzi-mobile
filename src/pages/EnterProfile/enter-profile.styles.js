import { StyleSheet } from 'react-native';
import { styles, colors } from '../../theme';

const AVATAR_SIZE = 100;

export default StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '60%',
        flex: 1,
    },
    avatar: {
        width: AVATAR_SIZE,
        height: AVATAR_SIZE,
        borderRadius: AVATAR_SIZE / 2,
    },
    textField: {
        width: '100%',
    }
});
