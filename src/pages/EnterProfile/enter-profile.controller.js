import React from 'react';

import EnterProfileScreen from './enter-profile.screen';

export default function () {
    return (
        <EnterProfileScreen />
    );
}
