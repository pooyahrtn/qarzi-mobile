import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { Avatar, KeyboardAvoiding, TextInput } from '../../components';
import styles from './enter-profile.styles';

export type Props = {

}
export default function EnterProfileScreen(props: Props) {
    return (
        <SafeAreaView style={styles.screen}>
            <KeyboardAvoiding style={styles.content} behavior="padding">
                <Avatar
                    containerStyle={styles.avatar}
                    
                />

                <TextInput
                    label="نام"
                    style={styles.textField}
                />
                <TextInput
                    label="نام خانوادگی"
                    style={styles.textField}
                />

            </KeyboardAvoiding>
        </SafeAreaView>
    );
}
