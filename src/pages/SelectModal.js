import React from 'react';
import {
    SafeAreaView,
    TouchableOpacity,
    StyleSheet,
    FlatList
} from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { Modal, Text } from '../components';
import { SelectModalParams } from '../router/navigate';
import { styles as gStyles } from '../theme';

type Props = {
    navigation: NavigationScreenProp<{}, SelectModalParams>
}
export default class SelectModal extends React.PureComponent<Props> {
    render() {
        const {
            navigation,
            navigation: {
                state: {
                    params: {
                        title,
                        options,
                        onSelect
                    }
                }
            }
        } = this.props;

        const onItemPressed = key => () => {
            onSelect(key);
            navigation.goBack();
        };

        return (
            <Modal
                navigation={navigation}
            >
                <SafeAreaView style={styles.container}>
                    <Text style={styles.titleText}>{title}</Text>
                    <FlatList
                        data={options}
                        style={{ marginBottom: 20 }}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                onPress={onItemPressed(item.key)}
                                style={styles.selectButton}
                            >
                                <Text style={styles.selectButtonText}>{item.value}</Text>
                            </TouchableOpacity>
                        )}
                    />
                </SafeAreaView>
            </Modal>
        );
    }
}

const marginHorizontal = 10;
const marginVertical = 13;
const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: 'white',
        ...gStyles.shadowStyles.small,
        ...gStyles.shadowStyles.center,
    },
    titleText: {
        fontSize: 16,
        // fontWeight: 'bold',
        marginVertical: marginHorizontal,
        marginHorizontal,
    },
    selectButton: {
        borderWidth: 1,
        borderRadius: 6,
        marginHorizontal,
        marginVertical: marginVertical / 2,
        padding: marginHorizontal,
        borderColor: 'rgb(200,200,200)',
    },
    selectButtonText: {
        textAlign: 'center',
        fontSize: 14,
    }
});
