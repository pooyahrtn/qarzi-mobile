import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import wrapContainers from '../containers/wrapContainers';
import { navigateToLoading } from '../router/navigate';
import AppContainer from '../containers/App/app.container';
import locales from '../locales';
import { Icon, Text, GradiantButton } from '../components';
import { colors, layout as gLayout } from '../theme';

type Props = {
  appContainer: AppContainer
};
function RequestAllowLocation(props: Props) {
  const {
    appContainer: { refreshLocation, setAllowLocation, setDontAskLocation },
    navigation
  } = props;

  const [loading, setLoading] = useState(false);

  const requestLocation = async () => {
    setLoading(true);
    await setAllowLocation(true);
    await refreshLocation();
    await setDontAskLocation(true);
    setLoading(false);
    navigateToLoading(navigation);
  };

  const askLater = async () => {
    await setAllowLocation(false);
    await setDontAskLocation(true);
    navigateToLoading(navigation);
  };

  return (
    <SafeAreaView style={styles.screen}>
      <Icon name="map-pin" iconType="feathers" size={100} color={colors.Secondary} />
      <Text style={styles.messageText}>{locales.requestLocation}</Text>
      <GradiantButton style={styles.submitButton} loading={loading} onPress={requestLocation}>
        <Text style={styles.submitText}>{locales.activate}</Text>
      </GradiantButton>
      <TouchableOpacity style={styles.submitButton} onPress={askLater}>
        <Text style={styles.laterText}>{locales.laterActivate}</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const SUBMIT_BUTTON = 40;
const styles = StyleSheet.create({
  screen: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  messageText: {
    margin: gLayout.largeMargin,
    textAlign: 'center',
    fontSize: 16
  },
  submitButton: {
    height: SUBMIT_BUTTON,
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    margin: gLayout.largeMargin,
    borderRadius: SUBMIT_BUTTON / 2
  },
  submitText: {
    color: 'white',
    // fontWeight: 'bold',
    fontSize: 18
  },
  laterText: {
    color: 'gray',
    textDecorationLine: 'underline'
  }
});

export default wrapContainers(
  RequestAllowLocation,
  {},
  {
    name: 'appContainer',
    container: AppContainer
  }
);
