import { NavigationScreenProp } from "react-navigation";
import { OutcomeSuggestsContainer } from "../../containers/Suggests/suggests.container";

export type LendModalItem = {
  id: String,
  user: { first_name: String, last_name: String },
  game: String,
  console: String,
  duration: String,
  price: String,
  name: String
};
export type LendModalParams = {
  item: LendModalItem
};

export type Props = {
  navigation: NavigationScreenProp<{}, LendModalParams>,
  container: OutcomeSuggestsContainer
};

export type LendModalViewProps = {
  item: LendModalItem,
  needIdCard: Boolean,
  navigation: NavigationScreenProp,
  loadingNewPost: Boolean,
  toggleNeedIdCard: () => void,
  onSubmitPressed: () => void,
  submitted: Boolean
};
