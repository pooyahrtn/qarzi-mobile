// @flow
import React, { useState, useEffect } from "react";
import LendModalView from "./lendModal.screen";
import { OutcomeSuggestsContainer } from "../../containers/Suggests/suggests.container";
import wrapContainer from "../../containers/wrapContainers";
import { Props } from "./lendModal.types";
import { showSuccessSnack } from "../../router/navigate";

function LendModalContainer(props: Props) {
  const {
    navigation,
    navigation: {
      state: {
        params: { item }
      }
    },
    container: {
      state: { loadingNewPost },
      addNewLendSuggest,
      isItemRequested
    }
  } = props;
  const [itemRequested, setItemRequested] = useState(false);
  const [needIdCard, setNeedIdCard] = useState(true);

  useEffect(() => {
    isItemRequested(item.id).then(isRequested => setItemRequested(isRequested));
  });

  const toggleNeedIdCard = () => setNeedIdCard(!needIdCard);
  const sendRequest = async () => {
    const res = await addNewLendSuggest({
      need_id: needIdCard,
      feed_id: item.id
    });
    if (res) {
      showSuccessSnack(navigation, () => navigation.goBack());
    }
  };

  return (
    <LendModalView
      item={item}
      navigation={navigation}
      needIdCard={needIdCard}
      loadingNewPost={loadingNewPost}
      toggleNeedIdCard={toggleNeedIdCard}
      onSubmitPressed={sendRequest}
      submitted={itemRequested}
    />
  );
}

export default wrapContainer(
  LendModalContainer,
  {},
  {
    name: "container",
    container: OutcomeSuggestsContainer
  }
);
