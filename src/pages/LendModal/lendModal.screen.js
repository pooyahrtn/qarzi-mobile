import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { LendModalViewProps } from './lendModal.types';
import { Text, Devider, Checkbox } from '../../components';
import SubmitModal from '../../components/MSubmitModal';
import { layout as gLayout, colors } from '../../theme';

function confirmText(game, gameConsole, duration, price, username) {
  return `آیا تمایل به اجاره دادن بازی
${game} 
برای کنسول ${gameConsole}
به مدت ${duration} 
به ازای مبلغ ${price} تومان
${username && `به ${username}`} دارید؟`;
}

function getName(first_name, last_name) {
  if (first_name && last_name) {
    return `${first_name} ${last_name}`;
  }
  if (first_name) {
    return first_name;
  }
  if (last_name) {
    return last_name;
  }
  return 'وی';
}

export default function LendModal(props: LendModalViewProps) {
  const {
    item: {
      user: { first_name, last_name },
      price,
      game,
      console: gameConsole,
      duration
    },
    navigation,
    needIdCard,
    toggleNeedIdCard,
    loadingNewPost,
    onSubmitPressed,
    submitted
  } = props;
  const needIdCardText = needIdCard ? 'دارم' : 'ندارم';
  const name = getName(first_name, last_name);
  return (
    <SubmitModal
      navigation={navigation}
      title="پیشنهاد اجاره"
      loadingPost={loadingNewPost}
      onSubmit={onSubmitPressed}
      submitted={submitted}
    >
      <Text style={styles.mainText} transformNumbers>
        {confirmText(game, gameConsole, duration, price, name)}
      </Text>
      <Devider style={{ marginVertical: 10 }} />
      <Text style={styles.mainText}>برای اطمینان بیشتر، میتوانید در خواست کارت ملی کنید</Text>
      <TouchableOpacity style={styles.needIdCardContainer} onPress={toggleNeedIdCard}>
        <Checkbox checked={needIdCard} activeColor={colors.Primary} />
        <Text style={styles.needIdCardText}>{`درخواست کارت ملی ${needIdCardText}`}</Text>
      </TouchableOpacity>
      <Text style={styles.warningText}>
        اپلیکیشن جارنت هیچگونه مسئولیتی در قبال معامله ی شما ندارد.
      </Text>
    </SubmitModal>
  );
}

const marginHorizontal = gLayout.mediumMargin;
const marginVertical = gLayout.largeMargin;
const styles = StyleSheet.create({
  mainText: {
    marginHorizontal: gLayout.largeMargin,
    lineHeight: 35
  },
  needIdCardContainer: {
    flexDirection: 'row-reverse',
    marginHorizontal,
    marginVertical,
    alignItems: 'center'
  },
  needIdCardText: {
    marginHorizontal
  },
  warningText: {
    color: 'gray',
    margin: marginVertical,
    fontSize: 14
  }
});
