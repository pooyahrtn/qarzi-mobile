import React from "react";
import { StyleSheet } from "react-native";
import SnackBar from "../components/SnackBar";
import { colors } from "../theme";
import locales from "../locales";

export default props => {
  const { navigation } = props;

  return (
    <SnackBar
      navigation={navigation}
      message={locales.anyError}
      containerStyle={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.Secondary
  }
});
