import React from "react";

import FeedScreen, { Props as FeedProps } from "../Abstract/Feeds/feeds.screen";
import RequestItem from "../../components/RequestItem";
import { transformLend } from "../../serializers/request.serializer";

type Props = FeedProps & {
  onLendButtonPressed: (item: any) => void,
  openFeedOptions: (item: any) => void
};
export default (props: Props) => {
  const {
    lat,
    long,
    myId,
    onLendButtonPressed,
    openFeedOptions,
    ...restProps
  } = props;
  return (
    <FeedScreen
      title="اجاره بده"
      renderItem={item => (
        <RequestItem
          data={transformLend(item, { lat, long }, myId)}
          actionButton={{
            title: "اجاره میدم",
            onPress: () => onLendButtonPressed(transformLend(item))
          }}
          onOptionPressed={() => openFeedOptions(item)}
        />
      )}
      {...restProps}
    />
  );
};
