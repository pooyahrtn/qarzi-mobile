import React, { useEffect } from 'react';
import LendScreen from './lend.screen';
import LendContainer from '../../containers/Lend/lend.container';
import AppContainer from '../../containers/App/app.container';
import wrapContainer from '../../containers/wrapContainers';
import { openAddNewPost, openLendModal, openFeedOptionModals } from '../../router/navigate';

type Props = {
  lendContainer: LendContainer,
  appStore: AppContainer
};

function LendScreenContainer(props: Props) {
  const {
    appStore: {
      state: { lat, long, id: myId },
      refreshLocation
    },
    lendContainer: {
      state: { refreshing, data },
      loadMore,
      refreshFeeds,
      initPage
    },
    navigation
  } = props;

  useEffect(() => {
    initPage();
    refreshLocation();
  }, []);

  const onAddButtonPressed = () => {
    openAddNewPost(navigation);
  };

  const onLendButtonPressed = item => {
    openLendModal(navigation, {
      item
    });
  };

  return (
    <LendScreen
      lat={lat}
      long={long}
      onAddButtonPressed={onAddButtonPressed}
      onLendButtonPressed={onLendButtonPressed}
      loadMore={loadMore}
      refreshFeeds={refreshFeeds}
      refreshing={refreshing}
      data={data}
      myId={myId}
      openFeedOptions={({ id }) => openFeedOptionModals(navigation, id, { isMine: false })}
    />
  );
}

export default wrapContainer(
  LendScreenContainer,
  {},
  {
    name: 'lendContainer',
    container: LendContainer
  },
  {
    name: 'appStore',
    container: AppContainer
  }
);
