import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { Modal, Text, LoadingButton } from '../components';
import { colors, layout, styles as gStyles } from '../theme';
import { reportFeed } from '../containers/AbstractFeed/abstract-feed.container';
import MyPostContainer from '../containers/MyPosts/my-posts.container';
import wrapContainer from '../containers/wrapContainers';
import locales from '../locales';
import { showSuccessSnack } from '../router/navigate';

type Props = {
  navigation: NavigationScreenProp,
  myPostsContainer: MyPostContainer
};

const FeedOptionModal = (props: Props) => {
  const {
    navigation,
    myPostsContainer: { deleteFeed }
  } = props;
  const id = navigation.getParam('id');
  const isMine = navigation.getParam('isMine');
  const [loadingDelete, setLoadingDelete] = useState(false);
  const [loadingReport, setloadingReport] = useState(false);
  const goBack = () => navigation.goBack();
  const onReportPressed = async () => {
    setloadingReport(true);
    await reportFeed(id);
    setloadingReport(false);
    goBack();
    showSuccessSnack(navigation);
  };

  const onDeletePressed = async () => {
    setLoadingDelete(true);
    const ok = await deleteFeed(id);
    setLoadingDelete(false);
    if (ok) {
      goBack();
      showSuccessSnack(navigation);
    }
  };

  return (
    <Modal navigation={navigation}>
      <SafeAreaView style={styles.container}>
        {isMine && (
          <LoadingButton style={styles.button} loading={loadingDelete} onPress={onDeletePressed}>
            <Text style={styles.reportText}>{locales.delete}</Text>
          </LoadingButton>
        )}
        {!isMine && (
          <LoadingButton style={styles.button} loading={loadingReport} onPress={onReportPressed}>
            <Text style={styles.reportText}>{locales.reportButtonText}</Text>
          </LoadingButton>
        )}

        <TouchableOpacity style={[styles.button, styles.cancelButton]} onPress={goBack}>
          <Text>{locales.cancel}</Text>
        </TouchableOpacity>
      </SafeAreaView>
    </Modal>
  );
};

export default wrapContainer(
  FeedOptionModal,
  {},
  {
    name: 'myPostsContainer',
    container: MyPostContainer
  }
);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center'
  },
  reportText: { color: colors.Secondary, fontSize: 17 },
  button: {
    width: '80%',
    minHeight: 55,
    borderRadius: layout.mediumMargin,
    marginHorizontal: layout.largeMargin,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    ...gStyles.shadowStyles.center,
    ...gStyles.shadowStyles.medium
  },
  cancelButton: {
    margin: layout.largeMargin
  }
});
