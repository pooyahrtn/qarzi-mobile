import React, { Fragment } from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import styles from './feeds.styles';
import { FloatingButton, Header } from '../../../components';
import { styles as gStyle } from '../../../theme';

export type Props = {
  data: [],
  refreshing: Boolean,
  refreshFeeds: () => void,
  loadMore: () => void,
  lat: Number,
  long: Number,
  onAddButtonPressed: () => void,
  title: String,
  renderItem: (item: Object) => React.PureComponent,
  EmptyComponent: React.FC
};
export default (props: Props) => {
  const {
    data,
    refreshFeeds,
    refreshing,
    lat,
    long,
    loadMore,
    onAddButtonPressed,
    title,
    renderItem,
    EmptyComponent
  } = props;
  return (
    <Fragment>
      <StatusBar backgroundColor="white" animated barStyle="dark-content" />
      <View style={styles.screen}>
        <Header title={title} />
        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          refreshing={refreshing}
          onRefresh={refreshFeeds}
          onEndReached={loadMore}
          extraData={{ lat, long }}
          renderItem={({ item }) => renderItem(item)}
          keyExtractor={item => item.id}
          onEndReachedThreshold={10}
          ListEmptyComponent={EmptyComponent}
        />
        <FloatingButton
          onPress={onAddButtonPressed}
          containerStyle={gStyle.FloatingButtonStyle.container}
        />
      </View>
    </Fragment>
  );
};
