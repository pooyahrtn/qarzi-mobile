import React, { useState } from 'react';
import firebase from 'react-native-firebase';
import { SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import * as Notification from '../storage/notification';
import wrapContainers from '../containers/wrapContainers';
import { navigateToLoading } from '../router/navigate';
import ProfileContainer from '../containers/Profile/profile.container';
import locales from '../locales';
import { Icon, Text, GradiantButton } from '../components';
import { colors, layout as gLayout } from '../theme';

type Props = {
  profileContainer: ProfileContainer
};
function RequestAllowNotificationPage(props: Props) {
  const [loading, setLoading] = useState(false);

  const {
    profileContainer: { updateProfile },
    navigation
  } = props;

  const requesNotification = async () => {
    setLoading(true);
    await firebase.messaging().requestPermission();
    await Notification.setNotificationSet(true);
    const fcmToken = await firebase.messaging().getToken();
    await updateProfile({ notification_token: fcmToken });
    setLoading(false);
    navigateToLoading(navigation);
  };

  const answerLater = async () => {
    setLoading(false);
    await Notification.setDontAskAgain(true);
    navigateToLoading(navigation);
  };

  return (
    <SafeAreaView style={styles.screen}>
      <Icon name="bell" iconType="feathers" size={100} color={colors.Secondary} />
      <Text style={styles.messageText}>{locales.requestNotification}</Text>
      <GradiantButton style={styles.submitButton} loading={loading} onPress={requesNotification}>
        <Text style={styles.submitText}>{locales.activate}</Text>
      </GradiantButton>
      <TouchableOpacity style={styles.submitButton} onPress={answerLater}>
        <Text style={styles.laterText}>{locales.laterActivate}</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const SUBMIT_BUTTON = 40;
const styles = StyleSheet.create({
  screen: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  messageText: {
    margin: gLayout.largeMargin,
    textAlign: 'center',
    fontSize: 16
  },
  submitButton: {
    height: SUBMIT_BUTTON,
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    margin: gLayout.largeMargin,
    borderRadius: SUBMIT_BUTTON / 2
  },
  submitText: {
    color: 'white',
    // fontWeight: 'bold',
    fontSize: 18
  },
  laterText: {
    color: 'gray',
    textDecorationLine: 'underline'
  }
});

export default wrapContainers(
  RequestAllowNotificationPage,
  {},
  {
    name: 'profileContainer',
    container: ProfileContainer
  }
);
