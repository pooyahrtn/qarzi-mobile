import React, { useEffect } from 'react';
import { Animated, StyleSheet, ViewStyle } from 'react-native';
import tagImage from '../../assets/tag.png';
import iconImage from '../../assets/icon.png';

const animatedValue = new Animated.Value(0);
const enterAnimatedValue = new Animated.Value(0);

type Props = {
  onAnimationEnd: () => void,
  containerStyle: ViewStyle
};
export default (props: Props) => {
  const { onAnimationEnd = () => {}, containerStyle } = props;
  useEffect(() => {
    Animated.sequence([
      Animated.spring(enterAnimatedValue, {
        delay: 400,
        toValue: 1,
        useNativeDriver: true
      }),
      Animated.spring(animatedValue, {
        delay: 1000,
        toValue: 1,
        useNativeDriver: true
      })
    ]).start(onAnimationEnd);
  });
  const reversedAnimatedValue = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 0]
  });

  const rotationAnimatedValue = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['40deg', '360deg']
  });
  return (
    <Animated.View
      style={[
        styles.container,
        {
          transform: [
            { rotate: rotationAnimatedValue },
            { scaleX: enterAnimatedValue },
            { scaleY: enterAnimatedValue }
          ],
          opacity: enterAnimatedValue
        },
        containerStyle
      ]}
    >
      <Animated.Image
        source={iconImage}
        style={[styles.image, styles.icon, { opacity: animatedValue }]}
        resizeMode="contain"
      />
      <Animated.Image
        source={tagImage}
        style={[styles.image, styles.tag, { opacity: reversedAnimatedValue }]}
        resizeMode="contain"
      />
    </Animated.View>
  );
};

const F_TAG = 1.505;
const F_ICON = 1.09;
const HEIGHT = 150;

const styles = StyleSheet.create({
  container: {
    height: HEIGHT,
    width: HEIGHT / 1.3
  },
  image: {
    height: '100%',
    bottom: 0,
    left: 0,
    position: 'absolute'
  },
  tag: {
    width: HEIGHT / F_TAG
  },
  icon: {
    width: HEIGHT / F_ICON
  }
});
