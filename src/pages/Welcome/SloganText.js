import React, { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import { Text } from '../../components';

const GIVE = 'بده'.split('');
const GET = 'بگیر'.split('');

const SHOW_STATE = {
  completed: 1500,
  inProgress: 300,
  clearing: 200
};

export default () => {
  const words = [GIVE, GET];
  const [currentWordIndex, setCurrentWordIndex] = useState(0);
  const [showState, setShowState] = useState(SHOW_STATE.inProgress);
  const [word, setWord] = useState('');
  useEffect(() => {
    const id = setTimeout(() => {
      const currentWord = words[currentWordIndex];
      if (word.length === currentWord.length && showState === SHOW_STATE.inProgress) {
        setShowState(SHOW_STATE.completed);
      } else if (showState === SHOW_STATE.completed) {
        setShowState(SHOW_STATE.clearing);
      } else if (showState === SHOW_STATE.clearing && word.length === 0) {
        setShowState(SHOW_STATE.inProgress);
        setCurrentWordIndex((currentWordIndex + 1) % 2);
      } else if (showState === SHOW_STATE.clearing) {
        setWord(currentWord.slice(0, word.length - 1).join(''));
      } else {
        setWord(currentWord.slice(0, word.length + 1).join(''));
      }
    }, showState);
    return () => {
      clearTimeout(id);
    };
  }, [currentWordIndex, showState, word]);
  return <Text style={styles.text}>اجاره {word}</Text>;
};

const styles = StyleSheet.create({
  text: {
    fontSize: 32
  }
});
