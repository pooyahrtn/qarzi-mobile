import AppContainer from '../../containers/App/app.container';
import wrapContainers from '../../containers/wrapContainers';
import WelcomeScreen from './welcome.screen';

export default wrapContainers(WelcomeScreen, {}, {
    name: 'app',
    container: AppContainer
});
