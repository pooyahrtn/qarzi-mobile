import React, { useState, useEffect } from 'react';
import { View, Image } from 'react-native';
import { View as AnimatedView } from 'react-native-animatable';
import { Text, DigitInput } from '../../components';
import styles from './welcome.styles';
import gradiant from '../../assets/gradient.png';
import Base from './Base';

const nDigits = 4;
const digitCellWidth = 40;

type Props = {
  confirmCode: String,
  setConfirmCode: (phone: String) => void,
  onOk: () => void,
  loading: Boolean,
  phone: String,
  confirmFailed: Boolean
};

export default (props: Props) => {
  const [showInput, setShowInput] = useState(false);
  // const [retry, setRetry] = useState(false);

  useEffect(() => {
    const tm = setTimeout(() => {
      setShowInput(true);
    }, 400);
    return () => clearTimeout(tm);
  }, []);

  // useEffect(() => {
  //     const tm = setTimeout(() => {
  //         setRetry(true);
  //     }, 2000);
  //     return () => clearTimeout(tm);
  // }, []);

  const { confirmCode, setConfirmCode, onOk, loading, onBack, phone, confirmFailed } = props;
  return (
    <Base
      okButtonText="تایید"
      images={[<Image source={gradiant} style={styles.veryCircleLeft} key="3" />]}
      loading={loading}
      onOk={onOk}
      onBack={onBack}
    >
      <View style={styles.loginContetnContainer}>
        {showInput && (
          <AnimatedView animation="fadeIn" style={styles.fieldContainer}>
            <Text style={styles.fieldTitle}>کد تایید سنجی</Text>
            {confirmFailed && <Text style={styles.fieldTitle}>کد وارد شده اشتباه است</Text>}
            <Text transformNumbers style={styles.messageText}>
              کد تایید به شماره ی{` ${phone} `}
              ارسال شد.
            </Text>
            <DigitInput
              codeLength={nDigits}
              cellWidth={digitCellWidth}
              value={confirmCode}
              autoFocus
              onChangeValue={setConfirmCode}
            />
            {/* {
                                retry && (
                                    <TouchableOpacity
                                        style={{
                                            padding: 10,
                                        }}
                                    >
                                        <Text style={styles.messageText}>ارسال مجدد</Text>
                                    </TouchableOpacity>
                                )
                            } */}
          </AnimatedView>
        )}
      </View>
    </Base>
  );
};
