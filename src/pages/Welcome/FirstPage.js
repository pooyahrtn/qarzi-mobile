import React, { useState } from 'react';
import { View, Image, Platform, UIManager, LayoutAnimation, StyleSheet } from 'react-native';
import Base from './Base';
import { Text } from '../../components';
import styles from './welcome.styles';
import gradiant from '../../assets/gradient.png';
import Logo from './Logo';
import SloganText from './SloganText';

const withLayoutAnimation = cb => {
  LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  cb();
};

export default props => {
  if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }
  const [showName, setShowName] = useState(false);
  const [showSlogan, setShowSlogan] = useState(false);
  const setShowNameTrue = () => {
    withLayoutAnimation(() => setShowName(true));
    setTimeout(() => withLayoutAnimation(() => setShowSlogan(true)), 1000);
  };
  const { onOk } = props;
  return (
    <Base
      onOk={showName && onOk}
      okButtonText="ادامه"
      images={[<Image source={gradiant} style={styles.bottomCircle} key="2" />]}
    >
      <View style={[styles.flexOne, styles.jCennter, styles.aCennter]}>
        <Logo onAnimationEnd={setShowNameTrue} />
        {showName && <Text style={iStyles.appName}>جارنت</Text>}
        {showSlogan && <SloganText />}
      </View>
    </Base>
  );
};

const iStyles = StyleSheet.create({
  appName: {
    margin: 15,
    fontSize: 25
  }
});
