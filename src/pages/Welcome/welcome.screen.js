import React, { useState, useEffect } from 'react';
import { Keyboard, Animated, Dimensions, Easing, StatusBar, BackHandler } from 'react-native';

import AppContainer from '../../containers/App/app.container';
import styles from './welcome.styles';
import FirstPage from './FirstPage';
import LoginPage from './LoginPage';
import ConfirmPage from './ConfirmCode';

import { toEnglish } from '../../utils/text';

const { height } = Dimensions.get('window');
const animationValue = new Animated.Value(0);

type Props = {
  app: AppContainer
};

export default (props: Props) => {
  const [page, setPage] = useState(0);
  const [phone, setPhone] = useState('۰۹');
  const [loginLoading, setLoginLoading] = useState(false);
  const [confirmCode, setConfirmCode] = useState('');
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [confirmFailed, setConfirmFailed] = useState(false);
  const {
    app: { requestLogin, requestConfirm }
  } = props;

  const {
    navigation: { navigate }
  } = props;

  const scrollPage = () => {
    Keyboard.dismiss();
    Animated.timing(animationValue, {
      toValue: -1 * (page + 1) * height,
      duration: 800,
      easing: Easing.quad,
      useNativeDriver: true
    }).start(() => {
      setPage(page + 1);
    });
  };

  const scrollUp = () => {
    Keyboard.dismiss();
    Animated.timing(animationValue, {
      toValue: (1 - page) * height,
      duration: 800,
      easing: Easing.quad,
      useNativeDriver: true
    }).start(() => {
      setPage(page - 1);
    });
  };

  useEffect(() => {
    const bl = BackHandler.addEventListener('hardwareBackPress', () => {
      if (page >= 1) {
        scrollUp();
        return true;
      }
      return false;
    });
    return () => bl.remove();
  }, [page]);

  const onLoginOk = async () => {
    const phoneNumber = toEnglish(phone);
    setLoginLoading(true);
    const res = await requestLogin(phoneNumber);
    setLoginLoading(false);
    if (res.ok) {
      scrollPage();
    }
  };

  const onConfirmOk = async () => {
    const _confirmCode = toEnglish(confirmCode);
    setConfirmLoading(true);
    const res = await requestConfirm(_confirmCode);
    setConfirmLoading(false);
    if (res.ok) {
      navigate('loading');
    } else if (res.status === 401) {
      setConfirmFailed(true);
    }
  };

  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <Animated.View
        style={[
          styles.screen,
          {
            transform: [
              {
                translateY: animationValue
              }
            ]
          }
        ]}
      >
        <FirstPage onOk={scrollPage} />

        {page >= 1 && (
          <LoginPage
            phone={phone}
            setPhone={setPhone}
            onOk={phone.length === 11 && onLoginOk}
            onBack={scrollUp}
            loading={loginLoading}
          />
        )}
        {page >= 2 && (
          <ConfirmPage
            confirmCode={confirmCode}
            setConfirmCode={setConfirmCode}
            onBack={scrollUp}
            onOk={confirmCode.length === 4 && onConfirmOk}
            loading={confirmLoading}
            phone={phone}
            confirmFailed={confirmFailed}
          />
        )}
      </Animated.View>
    </>
  );
};
