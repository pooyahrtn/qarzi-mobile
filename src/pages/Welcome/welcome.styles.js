import { StyleSheet, Dimensions } from 'react-native';
import { colors, layout, styles } from '../../theme';

const { height, width } = Dimensions.get('window');
const TOP_CIRCLE_SIZE = 160;
const BOTTOM_CIRCLE_SIZE = TOP_CIRCLE_SIZE * 0.85;

export default StyleSheet.create({
  screen: {
    height: 3 * height,
    backgroundColor: 'white'
  },
  flexOne: {
    flex: 1
  },
  jCennter: {
    justifyContent: 'center'
  },
  aCennter: {
    alignItems: 'center'
  },
  page: {
    width,
    height
  },
  topCircle: {
    position: 'absolute',
    width: 2 * TOP_CIRCLE_SIZE,
    height: 2 * TOP_CIRCLE_SIZE,
    borderRadius: TOP_CIRCLE_SIZE,
    top: -0.9 * TOP_CIRCLE_SIZE,
    left: -1 * TOP_CIRCLE_SIZE
  },
  bottomCircle: {
    position: 'absolute',
    width: 2 * BOTTOM_CIRCLE_SIZE,
    height: 2 * BOTTOM_CIRCLE_SIZE,
    borderRadius: BOTTOM_CIRCLE_SIZE,
    bottom: -1 * BOTTOM_CIRCLE_SIZE,
    right: -0.6 * BOTTOM_CIRCLE_SIZE
  },
  bottomCircleLeft: {
    position: 'absolute',
    width: 2 * BOTTOM_CIRCLE_SIZE,
    height: 2 * BOTTOM_CIRCLE_SIZE,
    borderRadius: BOTTOM_CIRCLE_SIZE,
    bottom: -0.6 * BOTTOM_CIRCLE_SIZE,
    right: -0.5 * BOTTOM_CIRCLE_SIZE
  },
  veryCircleLeft: {
    position: 'absolute',
    width: 2 * BOTTOM_CIRCLE_SIZE,
    height: 2 * BOTTOM_CIRCLE_SIZE,
    borderRadius: BOTTOM_CIRCLE_SIZE,
    bottom: -1 * BOTTOM_CIRCLE_SIZE,
    left: -0.7 * BOTTOM_CIRCLE_SIZE
  },
  continueButtonContainer: {
    backgroundColor: 'white',
    margin: layout.largeMargin,
    padding: layout.mediumMargin,
    height: 50,
    borderRadius: 30,
    minWidth: '30%',
    justifyContent: 'center',
    alignItems: 'center',
    ...styles.shadowStyles.center,
    ...styles.shadowStyles.medium
  },
  continueButtonText: {
    textAlign: 'center',
    color: colors.Secondary,
    // fontWeight: 'bold',
    fontSize: 17
  },
  fieldTitle: {
    marginVertical: layout.mediumMargin,
    fontSize: 18,
    color: colors.Secondary
  },
  fieldContainer: {
    backgroundColor: 'white',
    width: '100%',
    paddingHorizontal: 20,
    borderRadius: 15,
    alignItems: 'center'
  },
  loginContetnContainer: {
    justifyContent: 'flex-end',
    marginBottom: 30,
    alignItems: 'center',
    flex: 1
  },
  confirmButtonContainer: { alignItems: 'flex-end', minHeight: 110 },
  backButton: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'white',
    position: 'absolute',
    top: 50,
    left: 20,
    justifyContent: 'center',
    alignItems: 'center',
    ...styles.shadowStyles.small,
    ...styles.shadowStyles.center
  },
  messageText: {
    marginVertical: layout.mediumMargin,
    color: 'gray'
  }
});
