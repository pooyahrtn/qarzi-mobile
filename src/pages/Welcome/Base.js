import React from 'react';
import {
  View,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';
import { View as AnimatedView } from 'react-native-animatable';
import { LoadingButton, Text, Icon } from '../../components';
import styles from './welcome.styles';
import { colors } from '../../theme';

type Props = {
  children: any,
  onOk: () => void,
  okButtonText: String,
  images: Image[],
  loading: Boolean,
  onBack: () => void
};
export default (props: Props) => {
  const { children, onOk, okButtonText, images, loading, onBack } = props;
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={[styles.page]}>
        {images}

        <View style={[styles.flexOne]}>
          <KeyboardAvoidingView behavior="padding" style={[styles.flexOne]}>
            {children}

            <View style={styles.confirmButtonContainer}>
              {onOk && (
                <AnimatedView animation="fadeIn" delay={300} useNativeDriver>
                  <LoadingButton
                    loading={loading}
                    style={styles.continueButtonContainer}
                    onPress={onOk}
                  >
                    <Text style={styles.continueButtonText}>{okButtonText}</Text>
                  </LoadingButton>
                </AnimatedView>
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
        {onBack && (
          <TouchableOpacity style={styles.backButton} onPress={onBack}>
            <Icon name="arrow-up" iconType="feathers" color={colors.Secondary} />
          </TouchableOpacity>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};
