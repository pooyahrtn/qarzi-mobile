import React, { useState, useEffect } from 'react';
import { View, Image, Dimensions } from 'react-native';
import { View as AnimatedView } from 'react-native-animatable';
import { Text, DigitInput } from '../../components';
import styles from './welcome.styles';
import gradiant from '../../assets/gradient.png';
import Base from './Base';

const { width } = Dimensions.get('window');
const digitMargin = 2;
const digitCellWidth = (width - 40) / 11 - digitMargin;

type Props = {
  phone: String,
  setPhone: (phone: String) => void,
  onOk: () => void,
  loading: Boolean,
  onBack: () => void
};

export default (props: Props) => {
  const [showInput, setShowInput] = useState(false);

  useEffect(() => {
    const tm = setTimeout(() => {
      setShowInput(true);
    }, 400);
    return () => clearTimeout(tm);
  }, []);

  const { phone, setPhone, loading, onOk, onBack } = props;
  return (
    <Base
      okButtonText="تایید"
      images={[<Image source={gradiant} style={styles.bottomCircle} key="3" />]}
      loading={loading}
      onOk={onOk}
      onBack={onBack}
    >
      <View style={styles.loginContetnContainer}>
        {showInput && (
          <AnimatedView animation="fadeIn" useNativeDriver style={styles.fieldContainer}>
            <Text style={styles.fieldTitle}>شماره تماس</Text>
            <DigitInput
              codeLength={11}
              cellWidth={digitCellWidth}
              value={phone}
              autoFocus
              onChangeValue={setPhone}
            />
          </AnimatedView>
        )}
      </View>
    </Base>
  );
};
