import React from "react";
import { View, StyleSheet, ViewStyle } from "react-native";
import Modal, { ModalProps } from "react-native-modal";

type Props = {
  containerStyle: ViewStyle
};
export default function ModalView(props: ModalProps & Props) {
  const { children, containerStyle } = props;
  return (
    <Modal
      backdropTransitionOutTiming={0}
      animationInTiming={500}
      animationOutTiming={500}
      useNativeDriver
      backdropOpacity={0.55}
      {...props}
    >
      <View style={[styles.container, containerStyle]}>{children}</View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 15,
    backgroundColor: "white"
  }
});
