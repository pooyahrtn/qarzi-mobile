import React from 'react';
import {
    SafeAreaView,
    StyleSheet
} from 'react-native';
import { MaterialTopTabBarProps, MaterialTopTabBar } from 'react-navigation';
import { colors } from '../theme';

export default function (props: MaterialTopTabBarProps) {
    return (
        <SafeAreaView>
            <MaterialTopTabBar
                {...props}
                indicatorStyle={styles.indicator}
                activeTintColor={colors.Secondary}
                inactiveTintColor="gray"
                labelStyle={styles.labelStyle}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    textStyle: {
        textAlign: 'center',
    },
    indicator: {
        backgroundColor: colors.Secondary
    },
    tabStyle: {
        backgroundColor: 'white',
    },
    labelStyle: {
        fontSize: 14,
    }
});
