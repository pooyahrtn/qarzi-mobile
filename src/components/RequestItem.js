import React from 'react';
import { View, StyleSheet, ViewStyle } from 'react-native';
import { Text, Icon, UserRow, GradiantButton } from '.';
import { colors, styles as gStyles, layout as gLayout } from '../theme';

type RentRequestType = {
  user: {
    image: String,
    first_name: String,
    last_name: String
  },
  distance: Number,
  created_time: String,
  game: String,
  console: String,
  duration: String,
  price: Number,
  enabledAction: Boolean
};
export type Props = {
  data: RentRequestType,
  actionButton: {
    title: String,
    onPress: () => void
  },
  priceUnit: 'none' | 'perday',
  verbose: Boolean,
  type: 'bo' | 'le',
  containerStyle: ViewStyle,
  onOptionPressed: () => void,
  noUser: Boolean
};
export default class RequestItem extends React.PureComponent<Props> {
  render() {
    const {
      data: {
        user,
        distance,
        created_time,
        game,
        console: gameConsole,
        duration,
        price,
        enabledAction
      },
      actionButton,
      priceUnit,
      verbose,
      type,
      containerStyle,
      onOptionPressed,
      noUser
    } = this.props;
    // eslint-disable-next-line no-nested-ternary
    const intentionText = verbose && (type === 'le' ? 'میخوام' : 'میدم');
    return (
      <View style={containerStyle || styles.container}>
        {!noUser && (
          <UserRow
            containerStyle={styles.row}
            user={user}
            time={created_time}
            distance={distance}
            optionIconProps={detailIconProps}
            onOptionsPressed={onOptionPressed}
          />
        )}

        <Row iconName="disc">
          <Text style={[styles.titleText, styles.gameText]}>{`${game}`}</Text>
          {intentionText && (
            <Text style={[styles.titleText, styles.gameText]}>{` ${intentionText}`}</Text>
          )}
        </Row>
        <Row iconName="game-controller">
          <Text style={styles.titleText}>{gameConsole}</Text>
        </Row>
        {duration && (
          <Row iconName="calendar">
            <Text>به مدت</Text>
            <Text style={styles.titleText} transformNumbers>
              {` ${duration}`}
            </Text>
          </Row>
        )}
        <Row
          iconName="credit-card"
          left={
            actionButton &&
            enabledAction && (
              <GradiantButton style={styles.rentButton} onPress={actionButton.onPress}>
                <Text style={styles.rentText}>{actionButton.title}</Text>
              </GradiantButton>
            )
          }
        >
          <Text transformNumbers style={[styles.titleText, styles.priceText]}>
            {`${priceUnit === 'perday' ? 'روزی' : ''} ${price} تومان`}
          </Text>
        </Row>
        {/* <TouchableOpacity
                    style={styles.optionIconContainer}
                    onPress={() => { }}
                >
                    <Icon name="options" {...detailIconProps} />
                </TouchableOpacity> */}
      </View>
    );
  }
}

const detailIconProps = {
  size: 16,
  color: 'gray'
};

type RowProps = {
  iconName: String,
  left: View
};
function Row(props: RowProps) {
  const { iconName, children, left } = props;
  return (
    <View style={styles.row}>
      <Icon name={iconName} {...detailIconProps} />
      <View style={styles.titleContainer}>{children}</View>
      {left}
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row-reverse',
    minHeight: 35,
    alignItems: 'center',
    marginHorizontal: 5
  },
  container: {
    margin: 10,
    backgroundColor: 'white',
    ...gStyles.shadowStyles.small,
    ...gStyles.shadowStyles.center,
    paddingVertical: gLayout.largeMargin,
    paddingHorizontal: gLayout.mediumMargin,
    borderRadius: 15
  },
  gameText: {
    color: colors.Secondary,
    textTransform: 'uppercase'
  },
  titleText: {
    // fontWeight: 'bold',
    // fontSize: 15,
    // color: colors.Primary
    color: 'black'
  },
  priceText: {
    color: colors.Primary
  },
  titleContainer: {
    alignItems: 'center',
    flexDirection: 'row-reverse',
    flex: 1,
    flexWrap: 'wrap',
    marginHorizontal: 10
  },

  alignCenter: {
    alignItems: 'center'
  },
  rentButton: {
    borderRadius: 10,
    height: '90%',
    // marginHorizontal: 10,
    width: '30%'
  },
  rentText: {
    color: 'white',
    textAlign: 'center'
    // fontWeight: 'bold'
  },
  optionIconContainer: {
    padding: 10,
    position: 'absolute',
    top: gLayout.mediumMargin,
    left: gLayout.mediumMargin
  }
});
