/* eslint-disable react/destructuring-assignment */
import React from 'react';
import {
    TouchableOpacity,
    TouchableOpacityProps,
    StyleSheet,
    View,
    ActivityIndicator,
} from 'react-native';
import Image from 'react-native-fast-image';

const image = require('../assets/ggradiant.jpg');

type Props = {
    loading: Boolean
}

export default function (props: TouchableOpacityProps | Props) {
    const { children } = props;
    return (
        <TouchableOpacity
            style={[styles.container, props.style]}
            onPress={() => {
                if (!props.loading) {
                    props.onPress();
                }
            }}
        >
            <View style={styles.innerContainer}>
                <Image
                    style={[
                        styles.image]}
                    source={image}
                />

                {
                    props.loading ? (
                        <ActivityIndicator color="white" />

                    ) : (
                            children
                        )
                }
            </View>

        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        overflow: 'hidden',
    },
    innerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    text: {
        color: 'white',
        textAlign: 'center',
        // fontWeight: '100',
        fontSize: 18
    }
});
