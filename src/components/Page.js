import React from 'react';
import { View, StyleSheet } from 'react-native';
import Header, { Props as HeaderProps } from './Header';


type Props = {
    header: HeaderProps
}
export default function Page(props: Props) {
    const { header, children } = props;
    return (
        <View style={styles.screen}>
            <Header {...header} />
            <View style={styles.contentContainer}>
                {children}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        zIndex: 100,
    }
});
