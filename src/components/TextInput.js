import React from 'react';
import {
    StyleSheet, Platform
} from 'react-native';
import { TextField, TextFieldProps } from 'react-native-material-textfield';
import { colors } from '../theme';


// type Props = {
//     style: TextStyle,
//     transformNumbers: boolean,
// }
function CTextInput(props: TextFieldProps, ref) {
    const { style } = props;
    return (
        <TextField
            {...props}
            ref={ref}
            inputContainerStyle={[styles.inputContainer]}
            style={[styles.text, style]}
            affixTextStyle={styles.text}
            labelTextStyle={[styles.text]}
            titleTextStyle={styles.text}
            labelPadding={12}
            tintColor={colors.Secondary}
        />
    );
}

const styles = StyleSheet.create({
    inputContainer: {
        alignItems: 'flex-end',
    },
    text: {
        direction: 'rtl',
        textAlign: Platform.OS === 'ios' ? 'right' : 'auto',
        color: 'black',
        fontFamily: 'Behdad',
    }
});


export default React.forwardRef(CTextInput);
