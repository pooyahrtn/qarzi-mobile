import React from 'react';
import { View, Picker, StyleSheet } from 'react-native';
import moment from 'jalali-moment';

moment.locale('fa');

type Props = {
    value: Date,
    minAge: number,
    onValueChange: (newValue: Date) => void,
}


export default class DatePicker extends React.PureComponent<Props> {
    render() {
        const { value, minAge, onValueChange } = this.props;
        const [year, month, day] = getFromDate(value);

        return (
            <View style={styles.container}>
                <Picker
                    selectedValue={year}
                    style={styles.picker}
                    onValueChange={itemValue => onValueChange(convertToDate(itemValue, month, day))}
                >
                    {yearArray(minAge).map(item => (
                        <Picker.Item
                            label={String(item).toPersian()}
                            value={item}
                            key={item}
                        />
                    ))}
                </Picker>
                <Picker
                    selectedValue={month}
                    style={styles.picker}
                    onValueChange={itemValue => onValueChange(convertToDate(year, itemValue, day))}
                >
                    {[...Array(12)].map((_, index) => (
                        <Picker.Item
                            label={monthNames[index]}
                            value={index + 1}
                            key={String(index)}
                        />
                    ))}
                </Picker>
                <Picker
                    selectedValue={day}
                    style={styles.picker}
                    onValueChange={itemValue => (
                        onValueChange(convertToDate(year, month, itemValue))
                    )}
                >
                    {dayArray(month).map(item => (
                        <Picker.Item
                            label={String(item).toPersian()}
                            value={item}
                            key={item}
                        />
                    ))}
                </Picker>
            </View>
        );
    }
}


function convertToDate(jYear, jMonth, jDay) {
    return moment.from(`${jYear}/${jMonth}/${jDay}`, 'fa', 'YYYY/MM/DD').toDate();
}

function getFromDate(date: Date) {
    const m = moment(new Date(date));
    return [m.get('year'), m.get('month') + 1, m.get('D')];
}

function yearArray(minAge) {
    const currentYear = moment().get('year');
    return [...Array(60)].map((_, i) => currentYear - minAge - i);
}

function dayArray(currentMonth) {
    if (currentMonth <= 6) {
        return [...Array(31).keys()].map(item => item + 1);
    }
    return [...Array(30).keys()].map(item => item + 1);
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        // height,
    },
    picker: {
        width: 100,
        flex: 1,
    }
});


const monthNames = [
    'فروردین',
    'اردیبهشت',
    'خرداد',
    'تیر',
    'مرداد',
    'شهریور',
    'مهر',
    'آبان',
    'آذر',
    'دی',
    'بهمن',
    'اسفند'
];
