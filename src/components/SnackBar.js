import React, { useEffect } from "react";
import { SafeAreaView, StyleSheet, ViewStyle } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Modal, Text } from ".";
import { colors, layout } from "../theme";

type Props = {
  navigation: NavigationScreenProp,
  message: String,
  containerStyle: ViewStyle,
  onEnd: () => void
};

export default (props: Props) => {
  const { navigation, message, containerStyle, onEnd = () => {} } = props;
  useEffect(() => {
    const timeOutId = setTimeout(() => {
      navigation.goBack("");
      onEnd();
    }, 4000);
    return () => {
      clearTimeout(timeOutId);
    };
  });
  return (
    <Modal navigation={navigation}>
      <SafeAreaView style={[styles.container, containerStyle]}>
        <Text style={styles.text}>{message}</Text>
      </SafeAreaView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: colors.Secondary
  },
  text: { color: "white", margin: layout.largeMargin }
});
