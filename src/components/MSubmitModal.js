import React from "react";
import { View, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import Image from "react-native-fast-image";
import { Modal, Text, LoadingButton, Icon } from ".";
import { layout as gLayout, styles as gStyles, colors } from "../theme";

const gradiantImage = require("../assets/ggradiant.jpg");

type Props = {
  navigation: any,
  title: String,
  loadingPost: Boolean,
  onSubmit: () => void,
  submitted: Boolean
};

export default function MSubmitModal(props: Props) {
  const {
    navigation,
    children,
    title,
    loadingPost,
    onSubmit,
    submitted
  } = props;

  const submitButtonText = submitted
    ? "درخواست ارسال شده است"
    : "ارسال درخواست";
  return (
    <Modal navigation={navigation}>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.titleText}>{title}</Text>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
          >
            <Icon iconType="feathers" name="x" style={styles.closeIcon} />
          </TouchableOpacity>
        </View>
        {children}
        <SafeAreaView
          style={[
            styles.submitButtonContainer,
            submitted && styles.submittedContainer
          ]}
        >
          {!submitted && (
            <Image style={styles.gradiantImage} source={gradiantImage} />
          )}
          <LoadingButton
            style={[styles.loadingButton]}
            loading={loadingPost}
            onPress={submitted ? () => {} : onSubmit}
          >
            <Text style={styles.submitButtonText}>{submitButtonText}</Text>
          </LoadingButton>
        </SafeAreaView>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: "white",
    ...gStyles.shadowStyles.small,
    ...gStyles.shadowStyles.center
  },
  titleText: {
    margin: gLayout.largeMargin,
    // fontWeight: "bold",
    fontSize: 16
  },
  closeIcon: {
    margin: gLayout.largeMargin
  },
  gradiantImage: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  loadingButton: {
    minHeight: 60,
    paddingVertical: gLayout.largeMargin
  },
  submitButtonContainer: {
    ...gStyles.shadowStyles.small,
    shadowColor: colors.Primary,
    marginTop: gLayout.mediumMargin
  },
  submittedContainer: {
    backgroundColor: "gray"
  },
  submitButtonText: {
    color: "white",
    // fontWeight: "bold",
    fontSize: 16
  },
  headerContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: gLayout.mediumMargin
  }
});
