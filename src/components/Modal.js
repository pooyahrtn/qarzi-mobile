/* eslint-disable react/destructuring-assignment */
import React from 'react';
import {
    View, StyleSheet, TouchableWithoutFeedback
} from 'react-native';
// import { View as AnimatableView } from 'react-native-animatable';

type Props = {
    navigation: any,
    withShadow: Boolean,
}
export default function (props: Props) {
    const { withShadow } = props;
    return (
        <View
            style={[styles.container]}
        >
            <TouchableWithoutFeedback
                onPress={() => props.navigation.goBack()}
            >
                <View
                    style={
                        withShadow ? styles.touchableArea : styles.touchableAreaNoShadow}
                />
            </TouchableWithoutFeedback>
            {props.children}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
    },
    touchableArea: {
        flex: 1,
        backgroundColor: 'black',
    },
    touchableAreaNoShadow: {
        flex: 1,
        backgroundColor: 'transparent',
    }
});
