import React, { PureComponent } from 'react';
import { StyleSheet, View, TextInput, TouchableWithoutFeedback } from 'react-native';
import Text from './Text';

type Props = {
  codeLength: Number,
  value: String,
  cellWidth: Number,
  onChangeValue: (newValue: String) => void,
  margin: Number,
  height: Number,
  fontSize: Number,
  getFocus: (cb: () => void) => void,
  autoFocus: Boolean
};

export default class App extends PureComponent<Props> {
  // eslint-disable-next-line react/sort-comp
  constructor(props: Props) {
    super(props);
    const { getFocus } = props;
    if (getFocus) {
      getFocus(this.getFocuse);
    }
  }


  input = React.createRef();


  handleClick = () => {
    this.input.current.focus();
  };


  handleKeyPress = e => {
    const { value, onChangeValue } = this.props;
    if (e.nativeEvent.key === 'Backspace') {
      onChangeValue(value.slice(0, value.length - 1));
    }
  };

  handleChange = newValue => {
    const { codeLength, onChangeValue, value } = this.props;
    if (value.length >= codeLength) return null;
    return onChangeValue((value + newValue).slice(0, codeLength));
  };

  getFocuse = () => this.input.current.focus();

  render() {
    const {
      codeLength,
      value,
      cellWidth = 25,
      margin = 2,
      height = 40,
      fontSize = 15,
      autoFocus
    } = this.props;
    // const { focused } = this.state;

    const values = value.split('');

    const selectedIndex = values.length < codeLength ? values.length : codeLength - 1;

    const hideInput = !(values.length < codeLength);

    return (
      <TouchableWithoutFeedback onPress={this.handleClick}>
        <View style={[styles.wrap, { width: codeLength * (cellWidth + margin) }]}>
          <TextInput
            value=""
            ref={this.input}
            onChangeText={this.handleChange}
            onKeyPress={this.handleKeyPress}
            autoFocus={autoFocus}
            underlineColorAndroid="transparent"
            keyboardType="number-pad"
            enablesReturnKeyAutomatically
            style={[
              styles.input,
              {
                fontSize,
                width: cellWidth,
                left: selectedIndex * (cellWidth + margin),
                opacity: hideInput ? 0 : 1
              }
            ]}
          />
          {Array(codeLength)
            .fill(0)
            .map((v, index) => (
              <View
                style={[
                  styles.display,
                  {
                    width: cellWidth,
                    marginRight: margin,
                    height
                  }
                ]}
                key={String(index)}
                pointerEvents="none"
              >
                <Text style={[styles.text, { fontSize }]} transformNumbers>
                  {values[index] || ''}
                </Text>
              </View>
            ))}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    position: 'relative',
    flexDirection: 'row'
  },

  input: {
    position: 'absolute',
    textAlign: 'center',
    backgroundColor: 'transparent',
    top: 0,
    bottom: 0,
    color: 'white'
  },
  display: {
    marginRight: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
    borderBottomWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'visible'
  },
  text: {
    fontSize: 20
  }
  // noBorder: {
  //     borderRightWidth: 0,
  // },
  // shadows: {
  //     position: 'absolute',
  //     // left: -4,
  //     // top: -4,
  //     // bottom: -4,
  //     // right: -4,
  //     borderColor: 'rgba(58, 151, 212, 0.28)',
  //     borderWidth: 4,
  // },
});
