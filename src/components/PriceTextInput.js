import React from "react";
import { TextFieldProps } from "react-native-material-textfield";
import { Text, View, StyleSheet, Platform } from "react-native";
import { toPrice } from "../utils/text";
import TextInput from "./TextInput";

function convertNumbers(text) {
  const id = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
  return String(text).replace(/[0-9]/g, w => id[+w]);
}

export default (props: TextFieldProps) => {
  const { containerStyle, style, value, onChangeText } = props;

  const thisOnChangeText = (text: String) => {
    const realValue = text ? text.match(/[^,]/g).join("") : "";
    const toPersian = convertNumbers(realValue);
    onChangeText(toPrice(toPersian));
  };

  return (
    <View style={[containerStyle]}>
      <TextInput
        {...props}
        style={[styles.text, style, styles.textInput]}
        containerStyle={styles.textInputContainer}
        onChangeText={thisOnChangeText}
      />

      <Text style={[styles.text, styles.textOverlay]}>{value}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    textAlign: "left",
    letterSpacing: 4,
    fontFamily: Platform.OS === "ios" ? "Courier" : "monospace",
    color: "black",
    fontSize: 18
  },
  textInput: {
    color: "white"
  },
  textOverlay: {
    position: "absolute",
    left: 0,
    bottom: 17
  },
  textInputContainer: {
    width: "100%"
  }
});
