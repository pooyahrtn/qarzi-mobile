import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { SafeAreaView } from "react-navigation";
import Text from "./Text";
import { styles as gStyles } from "../theme";
import Icon from "./Icon";

export type Props = {
  title: String,
  back: {
    navigation: any,
    iconName: String,
    iconType: String,
    iconSize: Number
  },
  actionButton: React.FC
};
export default function Header(props: Props) {
  const { title, back, actionButton } = props;
  const goBack = () => {
    back.navigation.goBack();
  };
  return (
    <SafeAreaView style={styles.shadow}>
      <View style={styles.container}>
        <Text style={styles.textStyle}>{title}</Text>
        {back && (
          <TouchableOpacity style={styles.backButtonContainer} onPress={goBack}>
            <Icon
              name={back.iconName || "arrow-left"}
              iconType={back.iconType || "feathers"}
              size={back.iconSize || 23}
            />
          </TouchableOpacity>
        )}
        {actionButton}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row-reverse"
  },
  shadow: {
    ...gStyles.shadowStyles.small,
    ...gStyles.shadowStyles.center,
    backgroundColor: "white",
    zIndex: 400
  },
  textStyle: {
    fontSize: 20,
    // fontWeight: "bold",
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  backButtonContainer: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: "center",
    alignItems: "center"
  }
});
