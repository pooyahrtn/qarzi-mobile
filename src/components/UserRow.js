import React from 'react';
import { View, StyleSheet, ViewStyle, TouchableOpacity } from 'react-native';
import { IconProps } from 'react-native-vector-icons';
import ProgressiveImage from './ProgressiveImage';
import Text from './Text';
import Icon from './Icon';
import getImageThumbnail from '../utils/getThumbnailLocation';

export type Props = {
  user: {
    image: String,
    first_name: String,
    last_name: String
  },
  distance: Number,
  time: String,
  optionIconProps: IconProps,
  containerStyle: ViewStyle,
  onOptionsPressed: () => void
};
export default class UserRow extends React.PureComponent<Props> {
  render() {
    const { user, distance, time, optionIconProps, containerStyle, onOptionsPressed } = this.props;
    return (
      <View style={[styles.row, styles.container, containerStyle]}>
        {user && user.image && (
          <ProgressiveImage
            source={{ uri: user.image }}
            style={styles.avatar}
            thumbnailSource={{ uri: getImageThumbnail(user.image) }}
            containerStyle={styles.avatarContainer}
          />
        )}
        <View style={[styles.nameContainer]}>
          {user && !!user.first_name && !!user.last_name && (
            <Text style={styles.nameText}>{`${user.first_name} ${user.last_name}`}</Text>
          )}
          {(!!distance || !!time) && (
            <View style={[styles.row, { padding: 0 }]}>
              {!!distance && (
                <Text style={styles.distanceText} transformNumbers>
                  {`${distance} کیلومتر، `}
                </Text>
              )}
              {!!time && (
                <Text transformNumbers style={styles.distanceText}>
                  {`${time} پیش`}
                </Text>
              )}
            </View>
          )}
        </View>
        {onOptionsPressed && (
          <TouchableOpacity onPress={onOptionsPressed} style={styles.optionButton}>
            <Icon name="options" {...optionIconProps} />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
const AVATAR_SIZE = 48;
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row-reverse'
  },
  container: {
    marginVertical: 10
  },
  nameContainer: {
    flex: 1
  },
  nameText: {
    flex: 1,
    // fontWeight: "bold",
    textAlign: 'right'
  },
  distanceText: {
    color: 'gray',
    fontSize: 12
  },
  avatar: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE / 2
  },
  avatarContainer: { marginLeft: 10 },
  minHeight: { minHeight: AVATAR_SIZE },
  optionButton: {
    padding: 5,
    paddingHorizontal: 10
  }
});
