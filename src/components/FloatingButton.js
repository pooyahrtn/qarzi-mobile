import React from 'react';
import { TouchableOpacity, StyleSheet, ViewStyle } from 'react-native';
import Icon from './Icon';

type Props = {
    onPress: () => void,
    containerStyle: ViewStyle,
    iconName: String,
    iconType: String,
}
export default function FloatingButton(props: Props) {
    const {
        containerStyle,
        onPress,
        iconName,
        iconType
    } = props;
    return (
        <TouchableOpacity
            style={[styles.container, containerStyle]}
            onPress={onPress}
        >
            <Icon
                color="white"
                size={25}
                iconType={iconType || 'feathers'}
                name={iconName || 'plus'}
            />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    }
});
