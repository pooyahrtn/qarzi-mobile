import React from 'react';
import Icon from './Icon';

type Props = {
    checked: Boolean,
    activeColor: String,
}
export default function (props: Props) {
    const { checked, activeColor } = props;
    if (checked) {
        return (
            <Icon
                iconType="feathers"
                name="check-square"
                color={activeColor}
            />
        );
    }
    return (
        <Icon
            iconType="feathers"
            name="square"
        />
    );
}
