import React from 'react';
import { View, ViewStyle } from 'react-native';
import Image from 'react-native-fast-image';

type Props = {
    cropHeight: Number,
    cropWidth: Number,
    source: { uri: String },
    resizeMode: String,
    style: ViewStyle,
    width: Number,
    height: Number,
    cropTop: Number,
    cropLeft: Number,
}
export default function (props: Props) {
    const {
        cropHeight,
        cropWidth,
        style,
        cropLeft,
        cropTop,
        width,
        height,
        source,
        children,
        resizeMode
    } = props;
    return (
        <View style={[{
            overflow: 'hidden',
            height: cropHeight,
            width: cropWidth,
            backgroundColor: 'transparent'
        }, style]}
        >
            <Image
                style={{
                    position: 'absolute',
                    top: cropTop * -1,
                    left: cropLeft * -1,
                    width,
                    height,
                }}
                source={source}
                resizeMode={resizeMode}
            >
                {children}
            </Image>
        </View>
    );
}
