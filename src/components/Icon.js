/* eslint-disable react/destructuring-assignment */
import React from 'react';
import FeathersIcon from 'react-native-vector-icons/Feather';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Simplelineicons from 'react-native-vector-icons/SimpleLineIcons';
import { ViewStyle } from 'react-native';

type Props = {
    name: String,
    size: Number,
    color: String,
    iconType: 'feathers' | 'ion-icon' | 'simple-line',
    style: ViewStyle,
    offset: Number
}

export default function Icon(props: Props) {
    const { iconType } = props;
    const dOffset = 1;
    const iconProps = {
        size: props.size ? props.size : 24,
        name: props.name,
        style: {
            width: props.size ? props.size + dOffset : 24 + dOffset,
            height: props.size ? props.size + dOffset : 24 + dOffset,
            textAlign: 'center',
            ...props.style
        },
        color: props.color ? props.color : 'black',
    };
    if (iconType === 'ion-icon') {
        return <IonIcon {...iconProps} />;
    }
    if (iconType === 'feathers') {
        return <FeathersIcon {...iconProps} />;
    }
    return <Simplelineicons {...iconProps} />;
}
