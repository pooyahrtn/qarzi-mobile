import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  ViewStyle,
  View,
  ActivityIndicator,
  Platform
} from "react-native";
import Icon from "./Icon";
import ProgressiveImage from "./ProgressiveImage";

type Props = {
  image: string,
  containerStyle: ViewStyle,
  onPress: () => void,
  loading: boolean,
  iconSize: Number,
  thumbnailUri: String
};
export default function(props: Props) {
  const {
    containerStyle,
    image,
    onPress,
    loading,
    iconSize = 30,
    thumbnailUri
  } = props;
  const disabled = !onPress;

  if (image) {
    return (
      <TouchableOpacity
        style={[styles.container, containerStyle]}
        onPress={onPress}
        disabled={disabled}
      >
        <ProgressiveImage
          source={{ uri: image }}
          thumbnailSource={{ uri: thumbnailUri }}
          style={{
            width: containerStyle.width,
            height: containerStyle.height,
            borderRadius: containerStyle.borderRadius
          }}
        />
      </TouchableOpacity>
    );
  }
  if (loading) {
    return (
      <View style={[styles.container, containerStyle]}>
        <ActivityIndicator
          color="gray"
          size={Platform.OS === "android" ? 50 : 1}
        />
      </View>
    );
  }
  return (
    <TouchableOpacity
      style={[styles.container, containerStyle]}
      onPress={onPress}
      disabled={disabled}
    >
      <Icon name="user" size={iconSize} color="gray" iconType="feathers" />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "white",
    backgroundColor: "white",
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 0,
      height: 0
    },
    elevation: 1
  }
});
