import R from "ramda";

const reg = /(.+\.)(.+)/g;

export default (location: String) => {
  if (!location) {
    return null;
  }
  try {
    const [_, path, format] = reg.exec(location);
    return `${path}thumbnail.${format}`;
  } catch (e) {
    return null;
  }
};
