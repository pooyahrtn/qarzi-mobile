import requester, { Request } from "./request-with-auth";
import { RefreshTokenConfig } from "./refresh-token";

export default function createAPI(
  baseUrl: String,
  refreshTokenConfig: RefreshTokenConfig,
  onNotAuthorised: () => void,
  onInternnalError: () => void,
  onAnyError: () => void
) {
  return (req: Request) =>
    requester(
      baseUrl,
      refreshTokenConfig,
      onNotAuthorised,
      onInternnalError,
      onAnyError,
      req
    );
}
