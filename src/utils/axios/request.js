import Axios from 'axios';

export type Response = {
  ok: Boolean,
  status: Number,
  data: Object,
  error: String
};

export type FetchConfig = {
  url: String,
  method: 'GET' | 'POST' | 'PUT' | 'DELETE',
  body: Object,
  headers: Object
};

export default async function fetch(baseUrl: String, config: FetchConfig): Promise<Response> {
  const { body, url, method, headers } = config;
  try {
    const res = await Axios({
      baseURL: baseUrl,
      method,
      url,
      data: body,
      headers
    });

    return transformRes(res);
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      return transformServerError(error);
    }
    // The request was made but no response was received
    // `error.request` is an instance
    //  of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    return transformInternalError(error);
  }
}

function transformRes(res): Response {
  return {
    ok: res.status >= 200 && res.status <= 300,
    status: res.status,
    data: res.data,
    error: null,
    header: res.headers
  };
}

function transformServerError(error): Response {
  return {
    ok: false,
    error: error.response.data,
    status: error.response.status,
    data: null,
    header: error.response.headers
  };
}

function transformInternalError(error): Response {
  return {
    ok: false,
    error: error.request,
    status: null,
    data: null,
    header: null
  };
}
