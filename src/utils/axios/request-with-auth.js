import request, { FetchConfig } from './request';
import requestRefreshToken, { RefreshTokenConfig } from './refresh-token';
import * as tokenStore from './token-store';

export type Request =
  | {
      withAuth: Boolean
    }
  | FetchConfig;

async function requester(baseUrl: String, req: Request) {
  const { withAuth, ...fetchConfig } = req;
  const accessToken = await tokenStore.getAccessToken();
  const authHeaders = withAuth
    ? {
        Authorization: `Bearer ${accessToken}`
      }
    : {};

  return request(baseUrl, {
    ...fetchConfig,
    headers: {
      ...fetchConfig.headers,
      ...authHeaders
    }
  });
}

export default async function(
  baseUrl: String,
  refreshTokenConfig: RefreshTokenConfig,
  onNotAuthorised: () => void,
  onInternalError: () => void,
  onAnyError: () => void,
  req: Request
) {
  const res = await requester(baseUrl, req);
  if (res.status === 401) {
    // let's try refreshing the tokens
    const refreshToken = await tokenStore.getRefreshToken();
    const refreshTokenResponse = await requestRefreshToken(
      baseUrl,
      refreshToken,
      refreshTokenConfig
    );
    console.warn({ refreshTokenResponse });
    if (refreshTokenResponse.ok) {
      const newAccessToken = refreshTokenResponse.data[refreshTokenConfig.accessTokenKey];
      const newRefreshToken = refreshTokenResponse.data[refreshTokenConfig.refreshTokenKey];
      await tokenStore.setTokens(newAccessToken, newRefreshToken);
      return requester(baseUrl, req);
    }

    onNotAuthorised();
  }
  if (res.status === 500) {
    onInternalError();
  }
  if (res.status > 300 && res.status !== 401) {
    onAnyError();
  }
  return res;
}
