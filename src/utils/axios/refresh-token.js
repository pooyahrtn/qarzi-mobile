import request from './request';

export type RefreshTokenConfig = {
    url: String,
    accessTokenKey: String,
    refreshTokenKey: String,
};
export default async function getNewTokens(
    baseUrl,
    refreshToken,
    config: RefreshTokenConfig,
) {
    const {
        url,
    } = config;

    const res = await request(
        baseUrl,
        {
            url,
            method: 'POST',
            body: {
                refresh: refreshToken
            },
        }
    );
    return res;
}
