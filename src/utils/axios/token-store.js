import AsyncStorage from '@react-native-community/async-storage';

const REFRESH_KEY = 'REFRESH_KEY';
const ACCESS_KEY = 'ACCESS_KEY';

export function getRefreshToken() {
    return AsyncStorage.getItem(REFRESH_KEY);
}

export function getAccessToken() {
    return AsyncStorage.getItem(ACCESS_KEY);
}

export async function clearAll() {
    await AsyncStorage.removeItem(REFRESH_KEY);
    await AsyncStorage.removeItem(ACCESS_KEY);
}

export async function setTokens(access, refresh) {
    await AsyncStorage.setItem(ACCESS_KEY, access);
    await AsyncStorage.setItem(REFRESH_KEY, refresh);
}
