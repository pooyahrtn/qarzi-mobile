export function truncate(text: string, len: number) {
  if (text.length < len) {
    return text;
  }
  return `${text.substring(0, len)}...`;
}

export function toEnglish(text: String): String {
  const persianNumbers = [
    /۰/g,
    /۱/g,
    /۲/g,
    /۳/g,
    /۴/g,
    /۵/g,
    /۶/g,
    /۷/g,
    /۸/g,
    /۹/g
  ];
  const arabicNumbers = [
    /٠/g,
    /١/g,
    /٢/g,
    /٣/g,
    /٤/g,
    /٥/g,
    /٦/g,
    /٧/g,
    /٨/g,
    /٩/g
  ];
  let str = text;
  for (let i = 0; i < 10; i += 1) {
    str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
  }
  return str;
}

export function toPrice(text: String) {
  const _t = String(text);
  return _t.split(/(?=(?:...)*$)/).join(",");
}

export function parseNumber(text) {
  return parseInt(toEnglish(text), 10);
}

export function toPersian(text) {
  const id = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
  return String(text).replace(/[0-9]/g, w => id[+w]);
}
