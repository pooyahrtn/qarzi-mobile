import moment from 'moment';

export function timeTextDiff(date: String) {
    // eslint-disable-next-line no-underscore-dangle
    const _date = moment(date).toDate();
    // eslint-disable-next-line no-underscore-dangle
    const _now = new Date();
    // in minutes
    const duration = Math.round((_now.getTime() - _date.getTime()) / (1000 * 60));
    return durationText(duration);
}

export function durationText(minutes) {
    // less than hour
    if (minutes < 60) {
        return `${minutes} دقیقه`;
    }
    const hours = minutes / (60);
    // less than day
    if (hours < 24) {
        return `${Math.round(hours)} ساعت`;
    }
    const days = hours / 24;
    // less than
    if (days < 7) {
        return `${Math.round(days)} روز`;
    }
    const weeks = days / 7;
    if (weeks < 4) {
        return `${Math.round(weeks)} هفته`;
    }
    const months = weeks / 4;
    return `${Math.round(months)} ماه`;
}
