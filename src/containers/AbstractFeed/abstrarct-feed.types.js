export type FeedItem = {
  id: String,
  user: {
    image: String,
    first_name: String,
    last_name: String,
    id: String
  },
  lat: Number,
  long: Number,
  game: String,
  console: String,
  created_time: String
};

export type State<T> = {
  refreshing: Boolean,
  data: T[],
  loadingMore: Boolean,
  nextPage: Number,
  loadingNewPost: Boolean
};
