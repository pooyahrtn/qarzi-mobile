import Container from '../../utils/StoreContainer';
import { State } from './abstrarct-feed.types';
import api from '../../app/api';

export default class AbstractFeed<T> extends Container<State<T>> {
  state: State<T> = {
    refreshing: false,
    data: [],
    loadingMore: false,
    nextPage: null,
    loadingNewPost: false
  };

  refreshFeeds = async () => {
    this.currentPageRequest = null;
    await this.setState(
      {
        refreshing: true,
        loadingMore: false,
        nextPage: null
      },
      false
    );
    const res = await this.getFeeds();
    if (res.ok) {
      const {
        data: { results, next }
      } = res;
      await this.setState(
        {
          refreshing: false,
          data: results,
          nextPage: next
        },
        true
      );
    } else {
      await this.setState(
        {
          refreshing: false
        },
        true
      );
    }
  };

  initPage = async () => {
    await this.setState({ refreshing: true });
    await this.loadSavedData();
    await this.refreshFeeds();
  };

  loadMore = async () => {
    const { nextPage, data } = this.state;
    if (nextPage && this.currentPageRequest !== nextPage) {
      this.currentPageRequest = nextPage;
      await this.setState({
        loadingMore: true
      });
      const res = await this.getFeeds(nextPage);
      const {
        data: { results, next },
        ok
      } = res;
      if (ok) {
        await this.setState(
          {
            data: [...data, ...results],
            nextPage: next
          },
          true
        );
      }
    }
  };

  // eslint-disable-next-line no-unused-vars
  getFeeds = async (nextPage: String) => {};
}

export const reportFeed = async (id: String) =>
  api({
    url: '/feeds/report/',
    method: 'POST',
    withAuth: true,
    body: {
      feed_id: id
    }
  });
