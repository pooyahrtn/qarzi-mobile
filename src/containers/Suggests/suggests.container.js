import Container from '../AbstractFeed/abstract-feed.container';
import { SuggestsState, OutSuggestBorrowBody, OutSuggestLendBody } from './suggests.types';
import api from '../../app/api';

const getList = (url: String) => (page: String) =>
  api({
    url: page || url,
    method: 'GET',
    withAuth: true
  });

const suggestItemRequest = (url: String) => (body: OutSuggestBorrowBody | OutSuggestLendBody) =>
  api({ url, method: 'POST', withAuth: true, body });

const suggestRequest = (type: 'Borrow' | 'Lend') => {
  if (type === 'Borrow') {
    return suggestItemRequest('/suggests/suggest_borrow/');
  }
  return suggestItemRequest('/suggests/suggest_lend/');
};

const suggestLendRequest = suggestRequest('Lend');
const suggestBorrowRequest = suggestRequest('Borrow');

const getSuggest = (type: 'Income' | 'Outcome') => {
  if (type === 'Income') {
    return getList('/suggests/income/');
  }
  return getList('/suggests/outcome/');
};

export class OutcomeSuggestsContainer extends Container<SuggestsState> {
  getFeeds = getSuggest('Outcome');

  isItemRequested = async item_id => {
    await this.loadSavedData();
    const { data } = this.state;
    return !!data.find(item => item.feed.id === item_id);
  };

  addNewLendSuggest = (body: OutSuggestLendBody): Promise<Boolean> =>
    this.addNewRequest(suggestLendRequest)(body);

  addNewBorrowSuggest = (body: OutSuggestBorrowBody): Promise<Boolean> =>
    this.addNewRequest(suggestBorrowRequest)(body);

  addNewRequest = request => async <T>(body: T) => {
    await this.setState({ loadingNewPost: true });
    const res = await request(body);
    await this.setState({ loadingNewPost: false });
    if (res.ok) {
      const { data: newData } = res;
      const { data } = this.state;
      await this.setState({ data: [...data, newData] }, true);
    }
    console.warn(res);
    return res.ok;
  };
}

export class IncomeSuggestsContainer extends Container<SuggestsState> {
  getFeeds = getSuggest('Income');
}

export const outcomeInstance = new OutcomeSuggestsContainer({
  storeName: 'outcomeSuggest',
  persistKeys: ['data']
});

export const incomeInstance = new IncomeSuggestsContainer({
  storeName: 'incomeSuggests',
  persistKeys: ['data']
});
