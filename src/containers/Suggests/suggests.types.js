import { State } from "../AbstractFeed/abstrarct-feed.types";
import { LendType } from "../Lend/lend.types";
import { BorrowType } from "../Borrow/borrow.types";

export type IncomeSuggestType = {
  id: String,
  feed: LendType | BorrowType,
  type: "LendSuggest" | "BorrowSuggest",
  need_id: Boolean,
  from_user: any
};

export type SuggestsState = State<IncomeSuggestType>;

export type OutSuggestBorrowBody = {
  feed_id: String,
  duration: Number
};

export type OutSuggestLendBody = {
  feed_id: String,
  need_id: Boolean
};
