import Container from '../AbstractFeed/abstract-feed.container';
import { MyPostsState } from './my-posts.types';
import api from '../../app/api';

export default class MyPostsContainer extends Container<MyPostsState> {
  getFeeds = async (page: String) => {
    const res = await api({
      url: page || '/feeds/all/',
      method: 'GET',
      withAuth: true
    });
    return res;
  };

  deleteFeed = async (id: string) => {
    const res = await api({
      url: `/feeds/delete/${id}/`,
      method: 'DELETE',
      withAuth: true
    });
    if (res.ok) {
      const { data } = this.state;
      const newData = data.filter(item => item.id !== id);
      await this.setState({ data: newData }, true);
    }
    return res.ok;
  };
}

export const instance = new MyPostsContainer({
  storeName: 'MyPostsContainer',
  persistKeys: ['data']
});
