import { State } from "../AbstractFeed/abstrarct-feed.types";
import { BorrowType } from "../Borrow/borrow.types";
import { LendType } from "../Lend/lend.types";

export type MyPostType = {
  type: "BorrowFeed" | "LendFeed"
} & BorrowType &
  LendType;

export type MyPostsState = State<MyPostType>;
