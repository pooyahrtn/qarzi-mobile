import { BorrowType } from './borrow.types';
// eslint-disable-next-line import/prefer-default-export
export const data: BorrowType[] = [
    {
        user: {
            image: 'https://pooyaharatian.me/index_files/img_avatar.JPG',
            name: 'پویا هراتیان نژادی'
        },
        lat: 35.7301,
        long: 51.3723,
        time: new Date(2019, 5, 12),
        game: 'GTA V',
        console: 'PS4',
        pricePerDay: 2000,
        _id: 'sfea',
    },
    {
        user: {
            // image: 'https://pooyaharatian.me/index_files/img_avatar.JPG',
            name: 'سروش یوسفیان'
        },
        lat: 35.7201,
        long: 51.5723,
        time: new Date(2019, 5, 2),
        game: 'Red Dead Redemption Red Dead ',
        console: 'XBOX',
        pricePerDay: 4500,
        _id: 'sfwea',
    }
];
