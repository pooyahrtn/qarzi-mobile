import Container from "../AbstractFeed/abstract-feed.container";
import { BorrowState, BorrowType } from "./borrow.types";
import api from "../../app/api";

export default class BorrowContainer extends Container<BorrowState> {
  getFeeds = async (page: String) => {
    const res = await api({
      url: page || "/feeds/lend/",
      method: "GET"
    });
    return res;
  };

  addNewPost = async (data: BorrowType) => {
    const {
      lat,
      long,
      price_per_day,
      game,
      console: gameConsole,
      need_id
    } = data;
    await this.setState({ loadingNewPost: true });
    const res = await api({
      url: "/feeds/lend/",
      method: "POST",
      withAuth: true,
      body: {
        lat,
        long,
        price_per_day,
        game,
        console: gameConsole,
        need_id
      }
    });

    await this.setState({
      loadingNewPost: false
    });
    return res.ok;
  };
}

export const instance = new BorrowContainer({
  storeName: "BorroContainer",
  persistKeys: ["data"]
});
