import { State, FeedItem } from "../AbstractFeed/abstrarct-feed.types";

export type BorrowType = {
  price_per_day: Number,
  need_id: Boolean
} & FeedItem;

export type BorrowState = State<BorrowType>;
