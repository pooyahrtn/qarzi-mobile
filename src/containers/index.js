import { instance as borrowInstance } from "./Borrow/borrow.container";
import { instance as lendInstance } from "./Lend/lend.container";
import { instance as profileInstance } from "./Profile/profile.container";
import { instance as appInstance } from "./App/app.container";
import { instance as myPostInstance } from "./MyPosts/my-posts.container";
import { outcomeInstance, incomeInstance } from "./Suggests/suggests.container";

export default [
  borrowInstance,
  profileInstance,
  appInstance,
  lendInstance,
  myPostInstance,
  outcomeInstance,
  incomeInstance
];

export {
  appInstance,
  profileInstance,
  borrowInstance,
  lendInstance,
  myPostInstance,
  outcomeInstance,
  incomeInstance
};
