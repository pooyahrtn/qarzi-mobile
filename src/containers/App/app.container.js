import Geolocation from '@react-native-community/geolocation';
import Container from '../../utils/StoreContainer';
import { instance as profileContainer } from '../Profile/profile.container';
import { State } from './app.types';
import api, { setTokens } from '../../app/api';

export default class AppContainer extends Container<State> {
  state: State = {
    login: 'just-in',
    lat: undefined,
    long: undefined,
    username: undefined,
    id: undefined,
    allowedLocation: false,
    dontAskLocation: false
  };

  refreshLocation = async () => {
    await this.loadSavedData();
    const { allowedLocation } = this.state;
    if (!allowedLocation) {
      return;
    }
    if (this.lastUpdateTime && new Date().getTime() - this.lastUpdateTime.getTime() < 60 * 1000) {
      return;
    }
    Geolocation.getCurrentPosition(info => {
      this.lastUpdateTime = new Date();
      const {
        coords: { latitude, longitude }
      } = info;
      this.setState(
        {
          lat: latitude,
          long: longitude
        },
        true
      );
    });
  };

  requestLogin = async (phoneNumber: String) => {
    await this.setState(
      {
        username: `0098${phoneNumber.slice(1)}`
      },
      true
    );
    const res = await api({
      url: '/users/',
      method: 'POST',
      body: {
        username: `0098${phoneNumber.slice(1)}`
      }
    });
    return res;
  };

  setAllowLocation = (allowedLocation: Boolean) => this.setState({ allowedLocation }, true);

  setDontAskLocation = (dontAskLocation: Boolean) => this.setState({ dontAskLocation }, true);

  requestConfirm = async (confirmCode: String) => {
    const { username } = this.state;
    const res = await api({
      url: '/users/token/',
      method: 'POST',
      body: {
        username,
        password: confirmCode
      }
    });

    if (res.ok) {
      const {
        data: { refresh, access, user }
      } = res;
      await profileContainer.saveProfile(user);
      const { username: _username, id } = user;
      await this.setState({ login: 'signed-in', username: _username, id }, true);
      await setTokens(access, refresh);
    }
    return res;
  };

  logout = async () => {
    await this.setState({ login: 'just-in', username: null, id: null }, true);
  };
}

export const instance = new AppContainer({
  storeName: 'appContainer',
  persistKeys: ['login', 'lat', 'long', 'id', 'username', 'dontAskLocation', 'allowedLocation']
});
