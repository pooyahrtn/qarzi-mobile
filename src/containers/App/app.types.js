export type State = {
  login: 'anonymous' | 'signed-in' | 'just-in' | 'just-signed',
  lat: Number,
  long: Number,
  username: String,
  id: String,
  allowedLocation: Boolean,
  dontAskLocation: Boolean
};
