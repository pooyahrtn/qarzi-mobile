export type PublicUserType = {
    name: String,
    image: String,
};

export type IPost = {
    _id: String,
    time: String,
};

export type ILocation = {
    lat: Number,
    long: Number,
}

export type IUser = {
    user: PublicUserType,
}

export type IGame = {
    game: String,
    console: String,
};

export type ILend = {
    pricePerDay: Number,
    needIdCard: Boolean,
    bail: Number
}

export type IBorrow = {
    price: Number,
    duration: Number
}

export type IPublicRequest = IPost & ILocation & IUser & IGame;

export type IType = {
    type: 'bo' | 'le',
}
