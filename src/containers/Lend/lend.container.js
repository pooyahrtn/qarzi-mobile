import Container from "../AbstractFeed/abstract-feed.container";
import { LendState, LendType } from "./lend.types";
import api from "../../app/api";

export default class LendContainer extends Container<LendState> {
  getFeeds = async (page: String) => {
    const res = await api({
      url: page || "/feeds/borrow/",
      method: "GET"
    });
    return res;
  };

  addNewPost = async (data: LendType) => {
    const { lat, long, price, game, duration, console: gameConsole } = data;
    await this.setState({ loadingNewPost: true });
    const res = await api({
      url: "/feeds/borrow/",
      method: "POST",
      withAuth: true,
      body: {
        lat,
        long,
        price,
        game,
        duration,
        console: gameConsole
      }
    });
    // console.warn(res);
    await this.setState({
      loadingNewPost: false
    });
    return res.ok;
  };
}

export const instance = new LendContainer({
  storeName: "LendContainer",
  persistKeys: ["data"]
});
