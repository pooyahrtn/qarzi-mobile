import { State, FeedItem } from "../AbstractFeed/abstrarct-feed.types";

export type LendType = FeedItem & {
  duration: Number,
  price: Number
};

export type LendState = State<LendType>;
