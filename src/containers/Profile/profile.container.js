import Container from '../../utils/StoreContainer';
import { State, MyProfileType } from './profile.types';
// import * as dummy from "./dummy";
import api from '../../app/api';

export default class ProfileContainer extends Container<State> {
  state: State = {
    profile: {
      first_name: '',
      last_name: '',
      image: '',
      id: ''
    },
    loadingProfile: false,
    loadingSaveProfile: false
  };

  loadProfile = async () => {
    await this.loadSavedData();
    await this.setState({ loadingProfile: true });
    const res = await api({
      url: '/users/me/me/',
      method: 'GET',
      withAuth: true
    });
    await this.setState({ loadingProfile: false });
    if (res.ok) {
      const {
        data: { first_name, last_name, id, image }
      } = res;
      await this.setState(
        {
          profile: {
            first_name,
            last_name,
            id,
            image
          }
        },
        true
      );
    }
  };

  saveProfile = (data: MyProfileType) => this.setState({ profile: data }, true);

  updateProfilePicture = async (imagePath, mime) => {
    const {
      profile: { image, id, ...profileData }
    } = this.state;
    await this.setState({ profile: { ...profileData, image: imagePath } });
    // eslint-disable-next-line no-undef
    const form = new FormData();
    form.append('is_active', true);
    form.append('image', { uri: imagePath, type: mime, name: 'image.jpg' });

    const res = await api({
      url: `/users/me/${id}/`,
      method: 'PUT',
      body: form,
      withAuth: true,
      headers: { 'Content-Type': 'multipart/form-data' }
    });
    console.warn(res);

    if (res.ok) {
      const { image: newImage } = res.data;
      await this.setState({ profile: { ...profileData, image: newImage, id } }, true);
    } else {
      await this.setState({ profile: { ...profileData, image, id } }, true);
    }
  };

  updateProfile = async (data: MyProfileType) => {
    await this.loadSavedData();
    await this.setState({ loadingSaveProfile: true });
    const {
      profile: { id }
    } = this.state;
    const res = await api({
      url: `/users/me/${id}/`,
      method: 'PUT',
      body: data,
      withAuth: true
    });
    await this.setState({ loadingSaveProfile: false });
    if (res.ok) {
      const newData = res.data;
      await this.setState({ profile: newData }, true);
    }
    return res.ok;
  };

  deleteProfile = () => this.updateProfile({ is_active: false });

  uploadNotificationToken = (token: String) => {
    const {
      profile: { id }
    } = this.state;
    if (id) {
      return this.updateProfile({ notification_token: token });
    }
    return false;
  };
}

export const instance = new ProfileContainer({
  storeName: 'ProfileContainer',
  persistKeys: ['profile']
});
