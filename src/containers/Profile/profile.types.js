export type MyProfileType = {
  first_name: String,
  last_name: String,
  image: String,
  id: String,
  notification_token: string,
  is_active: boolean,
};

export type State = {
  profile: MyProfileType,
  loadingProfile: Boolean,
  refreshingMyPosts: Boolean,
  loadingMoreMyPosts: Boolean,
  loadingSaveProfile: Boolean
};
