import { useEffect } from 'react';
import firebase from 'react-native-firebase';

type Input = {
  uploadNotification: (token: String) => Promise<void>,
  onOpenNotification: () => void
};
export default function(input: Input) {
  const { uploadNotification, onOpenNotification } = input;
  useEffect(() => {
    const listenToTokens = firebase.messaging().onTokenRefresh(fcmToken => {
      uploadNotification(fcmToken);
    });
    const notificationOpenListener = firebase.notifications().onNotificationOpened(() => {
      onOpenNotification();
    });

    return () => {
      listenToTokens();
      notificationOpenListener();
    };
  }, []);
}
