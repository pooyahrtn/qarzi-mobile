/* eslint-disable no-unused-vars */
type Config = {
  url: string
};

const Local: Config = {
  url: 'http://192.168.2.123:3200/api/v1'
};

const Stage: Config = {
  url: 'https://jarent.app/api/v1'
};

export default Stage;
