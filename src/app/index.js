import React from 'react';
// import NotificationHandler from './notification';
import { onNavigationStateChange } from './analytic';
import {
  addOnNotAuthorisedListener,
  addOnInternalErrorListener,
  addOnAnnyErrorListener
} from './api';
import Router from '../router';
import { snacks, loading, tabs } from '../router/names';
import NavigationService from '../utils/NavigationService';
import withProvider from './withProvider';
import containers, { appInstance, profileInstance } from '../containers';
import useNotification from './useNotification';

const Entry = () => {
  useNotification({
    uploadNotification: profileInstance.uploadNotificationToken,
    onOpenNotification: navigateToNotificationTabs
  });
  return (
    <Router
      onNavigationStateChange={onNavigationStateChange}
      ref={navigatorRef => {
        NavigationService.setTopLevelNavigator(navigatorRef);
      }}
    />
  );
};

function onNotAuthorized() {
  console.warn('Logged Out Response');
  appInstance.setState({ login: 'just-in' }, true).then(() => {
    NavigationService.navigate(loading.loading);
  });
}

function onInternalError() {
  console.warn('Innternal Error');
}

function onAnyError() {
  NavigationService.navigate(snacks.error);
}

function navigateToNotificationTabs() {
  NavigationService.navigate(tabs.chat);
}

addOnNotAuthorisedListener(onNotAuthorized);
addOnInternalErrorListener(onInternalError);
addOnAnnyErrorListener(onAnyError);

export default withProvider(Entry, containers);
