import React from 'react';
import { Provider } from 'unstated';

export default (Children, containers) => props => (
    <Provider inject={containers}>
        <Children {...props} />
    </Provider>
);
