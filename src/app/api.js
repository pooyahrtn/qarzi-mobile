import requester from '../utils/axios';
import config from './config';

export { setTokens } from '../utils/axios/token-store';

const onNotAuthorizedListeners = [];
const onInternalErrorListeners = [];
const onAnyErrorListener = [];

function onNotAuthorised() {
  onNotAuthorizedListeners.forEach(cb => cb());
}

function onInternalError() {
  onInternalErrorListeners.forEach(cb => cb());
}

function onAnyError() {
  onAnyErrorListener.forEach(cb => cb());
}

export default requester(
  config.url,
  {
    url: '/users/token/refresh/',
    accessTokenKey: 'access',
    refreshTokenKey: 'refresh'
  },
  onNotAuthorised,
  onInternalError,
  onAnyError
);

export function addOnNotAuthorisedListener(cb) {
  onNotAuthorizedListeners.push(cb);
}

export function addOnInternalErrorListener(cb) {
  onInternalErrorListeners.push(cb);
}

export function addOnAnnyErrorListener(cb) {
  onAnyErrorListener.push(cb);
}
