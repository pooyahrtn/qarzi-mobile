/* eslint-disable camelcase */
import { MyPostType } from "../containers/MyPosts/my-posts.types";
import { Props as RequestItemViewProps } from "../components/RequestItem";

import { timeTextDiff, durationText } from "../utils/time";
import calcDistance from "../utils/distance";
import { toPrice } from "../utils/text";

const CONSOLES = {
  PS: "PS4",
  XB: "XBOX",
  PA: "PS3"
};

export default function transformRequest(request, location, myId) {
  const {
    lat,
    long,
    console: gameConsole,
    user: { id: userId }
  } = request;
  const enabledAction = myId !== userId;
  let distance;
  if (location) {
    distance = calcDistance(location.lat, location.long, lat, long);
  }
  const _console = CONSOLES[gameConsole];
  const created_time = timeTextDiff(request.created_time);

  return {
    ...request,
    distance,
    created_time,
    console: _console,
    enabledAction
  };
}

export function transformLend(request, location, myId) {
  const transformedRequest = transformRequest(request, location, myId);
  const { price, duration: plainDuration } = transformedRequest;
  const duration = durationText(plainDuration * 24 * 60);
  return {
    ...transformedRequest,
    price: toPrice(price),
    duration
  };
}

export function trasnformBorrow(
  request,
  location: { lat: Number, long: Number },
  myId
) {
  const transformedRequest = transformRequest(request, location, myId);
  const { price_per_day, need_id } = transformedRequest;

  return {
    ...transformedRequest,
    price: toPrice(price_per_day),
    needIdCard: need_id
  };
}

export function transformMyPost(request: MyPostType): RequestItemViewProps {
  const { type, ...requestBody } = request;
  if (type === "BorrowFeed") {
    return {
      data: transformLend(requestBody),
      type: "le",
      priceUnit: "none"
    };
  }
  return {
    data: trasnformBorrow(requestBody),
    type: "bo",
    priceUnit: "perday"
  };
}
