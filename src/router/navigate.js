// import { TextStyle } from "react-native";
import { modals, pageStack, loading, snacks } from './names';
import { LendModalParams } from '../pages/LendModal/lendModal.types';
import { BorrowModalParams } from '../pages/BorrowModal/borrowModal.types';

const {
  app: appRouteName,
  welcome: welcomeRouteName,
  enterProfile,
  loading: loadingRouteName,
  requestNotification,
  requestLocation
} = loading;

const { successSnack } = snacks;

const { selectModa, lendModal, borrowModal, feedOptionsModal } = modals;

const { newPost, settings } = pageStack;

function navigateTo(navigation, name, params) {
  navigation.navigate(name, params);
}

export function openAddNewPost(navigation) {
  navigateTo(navigation, newPost);
}

export type SelectModalParams = {
  title: String,
  options: {
    key: String,
    value: String
  }[],
  onSelect: (key: String) => void
};
export function openSelectModal(navigation, params: SelectModalParams) {
  navigateTo(navigation, selectModa, params);
}

export function openLendModal(navigation, params: LendModalParams) {
  navigateTo(navigation, lendModal, params);
}

export function openBorrowModal(navigation, params: BorrowModalParams) {
  navigateTo(navigation, borrowModal, params);
}

export function navigateToLoading(navigation) {
  navigateTo(navigation, loadingRouteName);
}

export function navigateToApp(navigation) {
  navigateTo(navigation, appRouteName);
}

export function navigateToWelcome(navigation) {
  navigateTo(navigation, welcomeRouteName);
}

export function navigateToEnterProfile(navigation) {
  navigateTo(navigation, enterProfile);
}

export function navigateToSettings(navigation) {
  navigateTo(navigation, settings);
}

export const openFeedOptionModals = (navigation, id, { isMine = false }) => {
  navigateTo(navigation, feedOptionsModal, { id, isMine });
};

export const showSuccessSnack = (navigation, onEnd) => {
  navigateTo(navigation, successSnack, { onEnd });
};

export function navigateToRequestNotification(navigation) {
  navigateTo(navigation, requestNotification);
}

export function navigateToRequestLocation(navigation) {
  navigateTo(navigation, requestLocation);
}
