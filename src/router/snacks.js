import { createStackNavigator } from 'react-navigation';
import { snacks } from './names';

import ErrorSnack from '../pages/ErrorSnackBar';
import SuccessSnack from '../pages/SuccessSnackBar';
import transitionConfig from './utils/cardTransitionConfig';

const { error, innerSnack, successSnack } = snacks;

export default innerSnackRoute =>
  createStackNavigator(
    {
      [error]: ErrorSnack,
      [successSnack]: SuccessSnack,
      [innerSnack]: innerSnackRoute
    },
    {
      headerMode: 'none',
      transparentCard: true,
      initialRouteName: innerSnack,
      mode: 'modal',
      cardStyle: {
        backgroundColor: 'transparent',
        opacity: 1
      },
      transitionConfig
    }
  );
