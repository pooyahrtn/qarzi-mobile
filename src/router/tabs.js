import React from "react";
import {
  createBottomTabNavigator,
  createMaterialTopTabNavigator
} from "react-navigation";
import { colors } from "../theme";
import { Icon } from "../components";
import { tabs } from "./names";

import LendScreen from "../pages/Lend";
import BorrowScreen from "../pages/Borrow";
import { OutComeSuggests, InComeSuggests } from "../pages/Chat";
import ProfileScreen from "../pages/Profile";
import TopTab from "../components/TopTab";

const chatNavigator = createMaterialTopTabNavigator(
  {
    outcome: {
      screen: OutComeSuggests,
      navigationOptions: () => ({
        title: "درخواست های من"
      })
    },
    income: {
      screen: InComeSuggests,
      navigationOptions: () => ({
        title: "درخواست دیگران"
      })
    }
  },
  {
    initialRouteName: "income",
    optimizationsEnabled: true,
    tabBarComponent: TopTab,
    tabBarOptions: {
      style: {
        backgroundColor: "white"
      }
    }
  }
);

export default createBottomTabNavigator(
  {
    [tabs.lend]: LendScreen,
    [tabs.borrow]: BorrowScreen,
    [tabs.chat]: chatNavigator,
    [tabs.profile]: ProfileScreen
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        const iconName = getIconName(routeName);
        return (
          <Icon
            name={iconName}
            size={25}
            color={tintColor}
            iconType="feathers"
          />
        );
      }
    }),
    tabBarOptions: {
      style: {
        borderTopColor: "transparent",
        shadowOpacity: 0.05,
        // backgroundColor: colors.secondaryColor,
        elevation: 5
      },
      showLabel: false,
      activeTintColor: colors.Secondary,
      inactiveTintColor: "rgb(170,170,170)"
    },
    initialRouteName: tabs.lend
  }
);

function getIconName(routeName) {
  switch (routeName) {
    case tabs.lend:
      return "arrow-up";
    case tabs.borrow:
      return "arrow-down";
    case tabs.newPost:
      return "plus";
    case tabs.chat:
      return "inbox";
    case tabs.profile:
      return "user";
    default:
      return "";
  }
}
