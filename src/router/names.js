export const loading = {
  loading: 'loading',
  app: 'app',
  welcome: 'welcome',
  requestNotification: 'requestNotification',
  requestLocation: 'requestLocation'
};

export const snacks = {
  innerSnack: 'innerSnack',
  successSnack: 'successSnack',
  error: 'error'
};

export const modals = {
  pageStack: 'pageStack',
  selectModa: 'selectModal',
  lendModal: 'lendModal',
  borrowModal: 'borrowModal',
  feedOptionsModal: 'feedOptionsModal'
};

export const pageStack = {
  tabs: 'tabs',
  newPost: 'newPost',
  settings: 'settings',
  enterProfile: 'enterProfile'
};

export const tabs = {
  borrow: 'borrow',
  chat: 'chat',
  lend: 'lend',
  newPost: 'newPost',
  profile: 'profile'
};
