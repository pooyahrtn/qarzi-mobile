import { createAppContainer } from "react-navigation";
import switchRoute from "./switch";
import withSnack from "./snacks";

export default createAppContainer(withSnack(switchRoute));
