import { createSwitchNavigator } from 'react-navigation';
import LoadingPage from '../pages/Loading';
import modals from './modals';
import { loading } from './names';
import WelcomePage from '../pages/Welcome';
import RequestNotificationPage from '../pages/RequestAllowNotification';
import RequestLocationPage from '../pages/RequestAllowLocation';

const {
  loading: loadingRouteName,
  app: appRouteName,
  welcome: welcomeRouteName,
  requestNotification: requestNotificationRouteName,
  requestLocation: requestLocationRouteName
} = loading;

export default createSwitchNavigator(
  {
    [loadingRouteName]: LoadingPage,
    [appRouteName]: modals,
    [welcomeRouteName]: WelcomePage,
    [requestNotificationRouteName]: RequestNotificationPage,
    [requestLocationRouteName]: RequestLocationPage
  },
  {
    initialRouteName: loadingRouteName
  }
);
