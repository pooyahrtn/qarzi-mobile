import { createStackNavigator } from 'react-navigation';
import { pageStack } from './names';
import NewPostScreen from '../pages/NewPost';
import SettingScreen from '../pages/Settings';
import EnterProfilePage from '../pages/EnterProfile';
import tabs from './tabs';

const { tabs: tabsRoute, newPost, settings, enterProfile } = pageStack;

export default createStackNavigator(
  {
    [tabsRoute]: tabs,
    [newPost]: NewPostScreen,
    [settings]: SettingScreen,
    [enterProfile]: EnterProfilePage
  },
  {
    headerMode: 'none',
    initialRouteName: tabsRoute,
    cardStyle: {
      opacity: 1
    },
    transitionConfig: () => ({
      screenInterpolator: sceneProps => {
        const { layout, position, scene } = sceneProps;
        const thisSceneIndex = scene.index;

        const height = layout.initHeight;
        const translateY = position.interpolate({
          inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
          outputRange: [height, 0, 0]
        });

        return { transform: [{ translateY }] };
      }
    })
  }
);
