import { createStackNavigator } from 'react-navigation';
import PageStack from './pageStack';
import { modals } from './names';
import SelectModal from '../pages/SelectModal';
import LendModal from '../pages/LendModal';
import BorrowModal from '../pages/BorrowModal';
// import ErrorSnack from "../pages/ErrorSnackBar";
import FeedOptionModal from '../pages/FeedOptionsModal';
import transitionConfig from './utils/cardTransitionConfig';
// import SuccessSnack from "../pages/SuccessSnackBar";

const {
  pageStack,
  selectModa,
  lendModal,
  borrowModal,
  feedOptionsModal
  // successSnack
} = modals;

export default createStackNavigator(
  {
    [pageStack._name]: PageStack,
    [selectModa]: SelectModal,
    [lendModal]: LendModal,
    [borrowModal]: BorrowModal,
    [feedOptionsModal]: FeedOptionModal
    // [successSnack]: SuccessSnack,
    // error: ErrorSnack
  },
  {
    headerMode: 'none',
    transparentCard: true,
    initialRouteName: pageStack._name,
    mode: 'modal',
    cardStyle: {
      backgroundColor: 'rgba(0,0,0,0)',
      opacity: 1
    },
    transitionConfig
  }
);
