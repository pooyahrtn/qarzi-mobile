module.exports = {
  extends: "airbnb",
  parser: "babel-eslint",
  env: {
    jest: true
  },
  rules: {
    "no-use-before-define": "off",
    "react/jsx-filename-extension": "off",
    "react/jsx-wrap-multilines": "off",
    "space-before-function-paren": "off",
    "react/jsx-one-expression-per-line": "off",
    "implicit-arrow-linebreak": "off",
    "react/prop-types": "off",
    "comma-dangle": "off",
    indent: "off",
    "react/jsx-indent": "off",
    "react/jsx-indent-props": "off",
    "no-underscore-dangle": "off",
    camelcase: "off",
    quotes: "off",
    "arrow-parens": "off",
    "object-curly-newline": "off",
    "operator-linebreak": "off"
  },
  globals: {
    fetch: false
  }
};
